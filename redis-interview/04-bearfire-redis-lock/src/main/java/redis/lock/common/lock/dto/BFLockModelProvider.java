package redis.lock.common.lock.dto;

import java.util.UUID;

import io.netty.util.internal.StringUtil;
import lombok.extern.slf4j.Slf4j;
import redis.lock.common.lock.annotation.BFLock;

/**
 * 将注解转换成对象
 */
@Slf4j
public class BFLockModelProvider {

    static ThreadLocal threadLocal = new ThreadLocal();

    /**
     * 转换成bflock 实体类
     * @param bfLock
     * @return
     */
    public static BFLockModel getInstence(BFLock bfLock) {
        if (threadLocal.get() == null) {
            // 没有数据则uuid
            if (StringUtil.isNullOrEmpty(bfLock.name())) {
                threadLocal.set(UUID.randomUUID());
            } else {
                threadLocal.set(bfLock.value());
            }
        }

        return new BFLockModel(bfLock.name(), threadLocal.get().toString(), getExpiteTime(bfLock));
    }

    public static Integer getExpiteTime(BFLock lock) {
        return lock.expireTime() == Integer.MIN_VALUE ? 60 : lock.expireTime();
    }
}
