package redis.lock.common.lock.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BFLockModel {

    private String  name;

    private String  value;

    private Integer expireTime;
}
