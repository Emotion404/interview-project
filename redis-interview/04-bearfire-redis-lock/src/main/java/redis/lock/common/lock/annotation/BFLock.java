package redis.lock.common.lock.annotation;

import java.lang.annotation.*;

/**
 * bearfire Lock
 */
@Target(value = {ElementType.METHOD})
@Retention(value = RetentionPolicy.RUNTIME)
@Documented
public @interface BFLock {

    /**
     * the lock name
     * @return String
     */
    String name();

    /**
     * the lock value
     * @return String
     */
    String value();

    /**
     * the expireTime that defaut 60
     * @return
     */
    int expireTime() default 60;
}
