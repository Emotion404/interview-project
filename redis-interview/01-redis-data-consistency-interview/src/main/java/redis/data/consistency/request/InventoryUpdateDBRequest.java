package redis.data.consistency.request;

import org.springframework.transaction.annotation.Transactional;

import redis.data.consistency.biz.InventoryProductBiz;
import redis.data.consistency.entity.InventoryProduct;

public class InventoryUpdateDBRequest implements Request {
    private InventoryProductBiz inventoryProductBiz;
    private InventoryProduct    inventoryProduct;

    public InventoryUpdateDBRequest(InventoryProduct inventoryProduct, InventoryProductBiz inventoryProductBiz) {
        this.inventoryProduct = inventoryProduct;
        this.inventoryProductBiz = inventoryProductBiz;
    }

    @Override
    @Transactional
    public void process() {
        inventoryProductBiz.removeInventoryProductCache(inventoryProduct.getProductId());
        inventoryProductBiz.updateInventoryProduct(inventoryProduct);
    }

    @Override
    public Integer getProductId() {
        // TODO Auto-generated method stub  
        return inventoryProduct.getProductId();
    }

    @Override
    public boolean isForceFefresh() {
        // TODO Auto-generated method stub  
        return false;
    }

}