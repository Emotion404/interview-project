package redis.data.consistency.request;

import redis.data.consistency.biz.InventoryProductBiz;
import redis.data.consistency.entity.InventoryProduct;

/** 
 ********************************************** 
 *  描述：查询缓存数据 
 *  1、从数据库中查询 
 *  2、从数据库中查询后插入到缓存中 
 * Simba.Hua 
 * 2017年8月30日 
 ********************************************** 
**/
public class InventoryQueryCacheRequest implements Request {
    private InventoryProductBiz inventoryProductBiz;
    private Integer             productId;
    private boolean             isForceFefresh;

    public InventoryQueryCacheRequest(Integer productId, InventoryProductBiz inventoryProductBiz, boolean isForceFefresh) {
        this.productId = productId;
        this.inventoryProductBiz = inventoryProductBiz;
        this.isForceFefresh = isForceFefresh;
    }

    @Override
    public void process() {
        InventoryProduct inventoryProduct = inventoryProductBiz.loadInventoryProductByProductId(productId);
        inventoryProductBiz.setInventoryProductCache(inventoryProduct);
    }

    @Override
    public Integer getProductId() {
        // TODO Auto-generated method stub  
        return productId;
    }

    public boolean isForceFefresh() {
        return isForceFefresh;
    }

    public void setForceFefresh(boolean isForceFefresh) {
        this.isForceFefresh = isForceFefresh;
    }

}