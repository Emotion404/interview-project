package redis.data.consistency.biz;

import org.springframework.stereotype.Component;

import redis.data.consistency.entity.InventoryProduct;

@Component
public class InventoryProductBiz {
    public InventoryProduct loadInventoryProductByProductId(Integer productId) {
        return new InventoryProduct();
    }

    public InventoryProduct loadInventoryProductCache(Integer productId) {

        return new InventoryProduct();
    }

    public void removeInventoryProductCache(Integer productId) {
    }

    public void updateInventoryProduct(InventoryProduct inventoryProduct) {
    }

    public void setInventoryProductCache(InventoryProduct inventoryProduct) {
    }
}
