package redis.data.consistency.request;

public interface Request {
    void process();

    Integer getProductId();

    boolean isForceFefresh();
}