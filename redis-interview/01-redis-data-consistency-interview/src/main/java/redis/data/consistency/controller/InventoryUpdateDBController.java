package redis.data.consistency.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import redis.data.consistency.biz.IRequestAsyncProcessBiz;
import redis.data.consistency.biz.InventoryProductBiz;
import redis.data.consistency.entity.InventoryProduct;
import redis.data.consistency.request.InventoryUpdateDBRequest;
import redis.data.consistency.request.Request;
import redis.data.consistency.response.Response;

/** 
 ********************************************** 
 *  描述：提交更新请求 
 * Simba.Hua 
 * 2017年9月1日 
 ********************************************** 
**/
@Controller("/inventory")
public class InventoryUpdateDBController {
    @Autowired
    private InventoryProductBiz     inventoryProductBiz;
    @Autowired
    private IRequestAsyncProcessBiz requestAsyncProcessBiz;

    @RequestMapping("/updateDBInventoryProduct")
    @ResponseBody
    public Response updateDBInventoryProduct(InventoryProduct inventoryProduct) {
        Request request = new InventoryUpdateDBRequest(inventoryProduct, inventoryProductBiz);
        requestAsyncProcessBiz.process(request);
        return new Response(Response.SUCCESS, "更新成功");
    }
}