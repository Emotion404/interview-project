package mq.demo01;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Send {

    private static final String QUEUE_NAME = "hello_world";

    public static void main(String[] args) throws IOException, TimeoutException {
        // 创建连接工厂
        ConnectionFactory connectionFactory = new ConnectionFactory();
        // 设置主机地址
        connectionFactory.setHost("localhost");
        connectionFactory.setPort(5672);
        // 创建一个新的连接
        Connection connection = connectionFactory.newConnection();
        // 创建一个通道
        Channel channel = connection.createChannel();
        // 通道连接队列
        // 指定一个队列，queueDeclare(String queue, boolean durable, boolean exclusive, boolean autoDelete, Map<String, Object> arguments)
        // queue: 队列名
        // durable: 是否持久化
        // exclusive: 仅创建者可以使用的私有队列，断开后自动删除
        // autoDelete: 当所有消费者断开连接之后是否自动删除队列
        // arguments
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);

        String message = "hi world!";
        // 发布信息 basicPublish(String exchange, String routingKey, BasicProperties props, byte[] body)
        // exchange: 交换机名
        // routingKey: 路由键
        // props: 消息的其他参数
        // body: 消息体
        channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
        System.out.println("[x] Send: '" + message + "'");
        channel.close();
        connection.close();
    }
}
