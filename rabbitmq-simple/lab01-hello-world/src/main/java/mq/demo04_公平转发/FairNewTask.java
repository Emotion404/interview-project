package mq.demo04_公平转发;

import com.rabbitmq.client.Channel;
import lombok.SneakyThrows;
import mq.common.ChannelFactory;

public class FairNewTask {
    private final static String QUEUE_NAME = "hello_world";

    @SneakyThrows
    public static void main(String[] args) {
        Channel channel = ChannelFactory.newInstance("localhost");
        // 指定一个队列
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        // 公平转发
        int prefetchCount = 1;
        channel.basicQos(prefetchCount);
        // 发送消息 
        for (int i = 10; i > 0; i--) {
            String message = "Liang:" + i;
            channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
            System.out.println(" [x] Sent '" + message + "'");
        }
        // 关闭频道和连接  
        ChannelFactory.closeChannel();
    }
}