package mq.demo05_发布订阅;

import com.rabbitmq.client.Channel;
import lombok.SneakyThrows;
import mq.common.ChannelFactory;


public class EmitLog {
    private static final String EXCHANGE_NAME = "logs";

    @SneakyThrows
    public static void main(String[] args) {
        Channel channel = ChannelFactory.newInstance("localhost");
        // 设置交换机 fanout 模式
        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
        // 发送消息  
        String message = "Liang-MSG log." + Thread.currentThread().getName();
        for (int i = 0; i < 10; i++) {
            String newMsg = message + i;
            channel.basicPublish(EXCHANGE_NAME, "", null, newMsg.getBytes());
            System.out.println(" [x] Sent '" + message + "'");
        }
        
        // 关闭频道和连接  
        ChannelFactory.closeChannel();
    }
}