package mq.demo03;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class AckNewTask {
    private final static String QUEUE_NAME = "hello_world";

    public static void main(String[] args) throws IOException, TimeoutException {
        // 创建连接工厂
        ConnectionFactory connectionFactory = new ConnectionFactory();
        // 设置主机地址
        connectionFactory.setHost("localhost");
        connectionFactory.setPort(5672);
        // 创建一个新的连接
        Connection connection = connectionFactory.newConnection();
        // 创建一个通道
        Channel channel = connection.createChannel();
        // 通道连接队列
        // 指定一个队列，queueDeclare(String queue, boolean durable, boolean exclusive, boolean autoDelete, Map<String, Object> arguments)
        // queue: 队列名
        // durable: 是否持久化
        // exclusive: 仅创建者可以使用的私有队列，断开后自动删除
        // autoDelete: 当所有消费者断开连接之后是否自动删除队列
        // arguments
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        // 发送消息 
        for (int i = 0; i < 10; i++) {
            String message = "Liang:" + i;
            channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
            System.out.println(" [x] Sent '" + message + "'");
        }
        // 关闭频道和连接  
        channel.close();
        connection.close();
    }
}