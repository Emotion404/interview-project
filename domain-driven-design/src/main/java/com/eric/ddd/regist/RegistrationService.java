package com.eric.ddd.regist;

import javax.xml.bind.ValidationException;

/**
 * @author eric
 * Description: 注册接口
 */
public interface RegistrationService {

    User register(String name, String phone, String address) throws ValidationException;
}
