package com.eric.ddd.snackmachine;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author eric
 * description:  自动售货机
 */
@Getter
@Setter
public class SnackMachine implements Serializable {

    private int oneCentCount;
    private int tenCentCount;
    private int quarterCentCount;
    private int oneDollarCount;
    private int tenDollarCount;
    private int quarterDollarCount;

    private int oneCentCountInTransaction = 0;
    private int tenCentCountInTransaction = 0;
    private int quarterCentCountInTransaction = 0;
    private int oneDollarCountInTransaction = 0;
    private int tenDollarCountInTransaction = 0;
    private int quarterDollarCountInTransaction = 0;

    /**
     * 塞钱进售货机
     *
     * @param oneCentCount       1分
     * @param tenCentCount       10分
     * @param quarterCentCount   15分
     * @param oneDollarCount     1元
     * @param tenDollarCount     10元
     * @param quarterDollarCount 15元
     */
    public void insertMoney(int oneCentCount,
                            int tenCentCount,
                            int quarterCentCount,
                            int oneDollarCount,
                            int tenDollarCount,
                            int quarterDollarCount) {
        this.oneCentCount += oneCentCount;
        this.tenCentCount += tenCentCount;
        this.quarterCentCount += quarterCentCount;
        this.oneDollarCount += oneDollarCount;
        this.tenDollarCount += tenDollarCount;
        this.quarterDollarCount += quarterDollarCount;
    }

    /**
     * 找钱
     */
    public void returnMoney() {
        oneCentCountInTransaction = 0;
        tenCentCountInTransaction = 0;
        quarterCentCountInTransaction = 0;
        oneDollarCountInTransaction = 0;
        tenDollarCountInTransaction = 0;
        quarterDollarCountInTransaction = 0;
    }

    /**
     * 购买零食
     */
    public void buySnack() {
        oneCentCount += oneCentCountInTransaction;
        tenCentCount += tenCentCountInTransaction;
        quarterCentCount += quarterCentCountInTransaction;
        this.oneDollarCount += oneDollarCountInTransaction;
        this.tenDollarCount += tenDollarCountInTransaction;
        this.quarterDollarCount += quarterDollarCountInTransaction;

        oneCentCountInTransaction = 0;
        tenCentCountInTransaction = 0;
        quarterCentCountInTransaction = 0;
        oneDollarCountInTransaction = 0;
        tenDollarCountInTransaction = 0;
        quarterDollarCountInTransaction = 0;

    }
}
