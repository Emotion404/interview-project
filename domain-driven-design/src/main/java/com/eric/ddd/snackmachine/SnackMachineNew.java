package com.eric.ddd.snackmachine;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author eric
 * description:  自动售货机
 */
@Getter
@Setter
public class SnackMachineNew implements Serializable {

    private Money moneyInside;
    private Money moneyInTransaction;


    public void insertMoney(Money money) {
        moneyInTransaction = Money.add(moneyInside, money);
    }

    /**
     * 找钱
     */
    public void returnMoney() {

    }

    /**
     * 购买零食
     */
    public void buySnack() {
        moneyInside = Money.add(moneyInside, moneyInTransaction);

    }
}
