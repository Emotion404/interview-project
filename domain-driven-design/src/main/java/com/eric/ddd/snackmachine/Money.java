package com.eric.ddd.snackmachine;

import java.io.Serializable;

/**
 * @author eric
 */
public class Money implements Serializable {
    private int oneCentCount;
    private int tenCentCount;
    private int quarterCentCount;
    private int oneDollarCount;
    private int tenDollarCount;
    private int quarterDollarCount;

    public Money(
            int oneCentCount,
            int tenCentCount,
            int quarterCentCount,
            int oneDollarCount,
            int tenDollarCount,
            int quarterDollarCount) {
        this.oneCentCount += oneCentCount;
        this.tenCentCount += tenCentCount;
        this.quarterCentCount += quarterCentCount;
        this.oneDollarCount += oneDollarCount;
        this.tenDollarCount += tenDollarCount;
        this.quarterDollarCount += quarterDollarCount;
    }

    /**
     * @param m1
     * @param m2
     * @return
     */
    public static Money add(Money m1, Money m2) {

        return new Money(m1.oneCentCount + m2.oneCentCount,
                m1.tenCentCount + m2.tenCentCount,
                m1.quarterCentCount + m2.quarterCentCount,
                m1.oneDollarCount + m2.oneDollarCount,
                m1.tenDollarCount + m2.tenDollarCount,
                m1.quarterDollarCount + m2.quarterDollarCount);
    }
}
