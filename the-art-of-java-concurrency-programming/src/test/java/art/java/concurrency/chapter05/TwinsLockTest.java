package art.java.concurrency.chapter05;

import art.java.concurrency.chapter04.SleepUtils;
import org.junit.Test;

import java.util.concurrent.locks.Lock;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * Description: TwinsLock测试
 */
public class TwinsLockTest {

    @Test
    public void test() {
        final Lock lock = new TwinsLock();

        class Worker extends Thread {
            @Override
            public void run() {
                while (true) {
                    lock.lock();
                    try {
                        SleepUtils.second(1);
                        System.out.println(Thread.currentThread().getName());
                        SleepUtils.second(1);
                    } finally {
                        lock.unlock();
                    }
                }
            }
        }

        for (int i = 0; i < 50; i++) {
            Worker worker = new Worker();
            worker.setName("currentThread-" + i);
            worker.setDaemon(true);
            worker.start();
        }

        for (int i = 0; i < 50; i++) {
            SleepUtils.millis(2000);
            System.out.println();
        }
    }
}
