package com.spirit.concurrency.semaphore;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * @auth eric-好好学习，天天向上!
 * @date 2022/4/1
 * @description 信号量测试
 */
public class SemaphoreTest {

    static int count;

    static final Semaphore semaphore = new Semaphore(10);

    static void addOne() throws InterruptedException {
        semaphore.acquire();
        try {
            count += 1;
            TimeUnit.MILLISECONDS.sleep(1);
        } finally {
            System.out.println("count = " + count);
            semaphore.release();
        }
    }

    public static void main(String[] args) {
        for (int i = 0; i < 14; i++) {
            new Thread(() -> {
                try {
                    addOne();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }).start();
        }
    }

}
