package chapter9.service;

/**
 * @author eric
 * Description: 条件
 */
public interface Predicate {

    /**
     * 判断条件是否满足
     *
     * @return true/false
     */
    boolean evaluate();
}
