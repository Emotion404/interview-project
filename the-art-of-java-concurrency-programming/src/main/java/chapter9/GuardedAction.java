package chapter9;

import chapter9.service.Predicate;

import java.util.concurrent.Callable;

/**
 * @param <V> Description: 抽象目标动作，内含所需保护条件
 * @author eric
 */
public abstract class GuardedAction<V> implements Callable<V> {

    protected final Predicate predicate;

    public GuardedAction(Predicate predicate) {
        this.predicate = predicate;
    }

}
