*线程状态*
![线程状态图](images/线程状态.png)

![线程状态流转图](images/线程状态流转.png)
- 初始化： new Thread()
- 就绪： 
   - Thread.start()
   - 当前线程sleep()方法结束，
   - 其他线程join()结束，等待用户输入完毕，
   - 某个线程拿到对象锁
   - 当前线程时间片用完了，调用当前线程的yield()方法
   - 锁池里的线程拿到对象锁后，进入就绪状态
- 运行中： 
  - 线程调度程序从可运行池中选择一个线程作为当前线程时线程所处的状态。这也是线程进入运行状态的唯一一种方式
- 等待：
  - sleep()
  - wait()
- 等待超时
  - 调用sleep或是wait方法后线程处于TIMED_WAITING（等待超时）状态。 等待被唤醒或时间超时自动唤醒
- 终止： 
  - run() 方法结束 或 main() 结束
  - 在一个终止的线程上调用start()方法，会抛出java.lang.IllegalThreadStateException异常