package art.java.concurrency.chapter04;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * Description:
 */
public class Sychronized {

    public static void main(String[] args) {
        // 对 Sychronized.class 加锁
        synchronized (Sychronized.class) {

        }

        // 静态同步方法，对Synchronized Class 对象加锁
        m();
    }

    public static synchronized void m() {

    }
}

