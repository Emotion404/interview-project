package art.java.concurrency.chapter04;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * Description: 一个java进程包含哪些线程
 */
public class MultiThread {

    public static void main(String[] args) {
        // 获取Java线程管理MXBean 线程管理的Bean
        ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
        // 不需要获取同步的monitor 和 sychronizer信息， 仅获取线程和线程堆栈信息
        ThreadInfo[] threadInfos = threadMXBean.dumpAllThreads(false, false);
        // 遍历线程信息，打印线程id和线程名称
        for (ThreadInfo threadInfo : threadInfos) {
            System.out.println("[" + threadInfo.getThreadId() + "] " + threadInfo.getThreadName());
        }
    }

    /**
     *  打印信息
     * [8] JDWP Command Reader
     * [7] JDWP Event Helper Thread
     * [6] JDWP Transport Listener: dt_socket
     * [5] Attach Listener
     * [4] Signal Dispatcher
     * [3] Finalizer
     * [2] Reference Handler
     * [1] main
     */
}
