package art.java.concurrency.chapter04.demo.threadpool;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * Description: 线程池接口
 */
public interface ThreadPool<Job extends Runnable> {

    /**
     * 执行一个job 这个job需要实现Runnable接口
     *
     * @param job 一个线程job
     */
    void execute(Job job);

    /**
     * 关闭线程池
     */
    void shutdown();

    /**
     * 增加工作者线程
     *
     * @param num 数量
     */
    void addWorkers(int num);

    /**
     * 删除工作者线程
     *
     * @param num 数量
     */
    void removeWorkers(int num);

    /**
     * 获取正在等待执行的任务数量
     *
     * @return 任务数量
     */
    int getJobSize();
}
