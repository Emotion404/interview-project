package art.java.concurrency.chapter04;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * Description: 线程的状态 NEW RUNNABLE BLOCKED WAITING TIME_WAITING TERIMAL
 */
public class ThreadState {

    public static void main(String[] args) {
        new Thread(new TimeWaiting(), "TimeWaitingThread").start();
        new Thread(new Waiting(), "WaitingThread").start();
        // 使用两个Blocked 线程， 当一个获取锁后， 另一个被阻塞
        new Thread(new Blocked(), "BlockThread-1").start();
        new Thread(new Blocked(), "BlockThread-2").start();
    }

    /**
     * 该线程不断进行睡眠
     */
    static class TimeWaiting implements Runnable {

        @Override
        public void run() {
            while (true) {
                SleepUtils.second(100);
            }
        }
    }

    /**
     * 该线程在Waiting.class 上等待
     */
    static class Waiting implements Runnable {

        @Override
        public void run() {
            while (true) {
                synchronized (Waiting.class) {
                    try {
                        Waiting.class.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * 该线程在Block.class实例上加锁喉，不会释放该锁
     */
    static class Blocked implements Runnable {

        @Override
        public void run() {
            while (true) {
                synchronized (Blocked.class) {
                    SleepUtils.second(100);
                }
            }
        }
    }
}

/**
 * "BlockThread-2" #23 prio=5 os_prio=0 tid=0x000000001e430000 nid=0x12f30 waiting for monitor entry [0x000000002118f000]
 * java.lang.Thread.State: BLOCKED (on object monitor)
 * at art.java.concurrency.chapter04.ThreadState$Blocked.run(ThreadState.java:61)
 * - waiting to lock <0x000000076e185488> (a java.lang.Class for art.java.concurrency.chapter04.ThreadState$Blocked)
 * at java.lang.Thread.run(Thread.java:748)
 * <p>
 * "BlockThread-1" #22 prio=5 os_prio=0 tid=0x000000001e420800 nid=0x2dac waiting on condition [0x000000002108e000]
 * java.lang.Thread.State: TIMED_WAITING (sleeping)
 * at java.lang.Thread.sleep(Native Method)
 * at java.lang.Thread.sleep(Thread.java:340)
 * at java.util.concurrent.TimeUnit.sleep(TimeUnit.java:386)
 * at art.java.concurrency.chapter04.SleepUtils.second(ThreadState.java:71)
 * at art.java.concurrency.chapter04.ThreadState$Blocked.run(ThreadState.java:61)
 * - locked <0x000000076e185488> (a java.lang.Class for art.java.concurrency.chapter04.ThreadState$Blocked)
 * at java.lang.Thread.run(Thread.java:748)
 * <p>
 * "WaitingThread" #21 prio=5 os_prio=0 tid=0x000000001e41f800 nid=0x136c4 in Object.wait() [0x0000000020f8f000]
 * java.lang.Thread.State: WAITING (on object monitor)
 * at java.lang.Object.wait(Native Method)
 * - waiting on <0x000000076e181eb8> (a java.lang.Class for art.java.concurrency.chapter04.ThreadState$Waiting)
 * at java.lang.Object.wait(Object.java:502)
 * at art.java.concurrency.chapter04.ThreadState$Waiting.run(ThreadState.java:43)
 * - locked <0x000000076e181eb8> (a java.lang.Class for art.java.concurrency.chapter04.ThreadState$Waiting)
 * at java.lang.Thread.run(Thread.java:748)
 * <p>
 * "TimeWaitingThread" #20 prio=5 os_prio=0 tid=0x000000001e41e800 nid=0x1169c waiting on condition [0x0000000020e8f000]
 * java.lang.Thread.State: TIMED_WAITING (sleeping)
 * at java.lang.Thread.sleep(Native Method)
 * at java.lang.Thread.sleep(Thread.java:340)
 * at java.util.concurrent.TimeUnit.sleep(TimeUnit.java:386)
 * at art.java.concurrency.chapter04.SleepUtils.second(ThreadState.java:71)
 * at art.java.concurrency.chapter04.ThreadState$TimeWaiting.run(ThreadState.java:28)
 * at java.lang.Thread.run(Thread.java:748)
 */

