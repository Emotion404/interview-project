package art.java.concurrency.chapter04.demo.connectionpool;

import java.sql.Connection;
import java.util.LinkedList;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * Description: 连接池对象
 */
public class ConnectionPool {
    private LinkedList<Connection> pool = new LinkedList<>();

    /**
     * 创建连接池
     *
     * @param initialSize 初始化连接池大小
     */
    public ConnectionPool(int initialSize) {
        if (initialSize > 0) {
            // 创建数据库连接池
            for (int i = 0; i < initialSize; i++) {
                pool.add(ConnectionDriver.createConnection());
            }
        }
    }

    /**
     * 释放数据库连接
     *
     * @param connection 数据库连接对象
     */
    public void releaseConnection(Connection connection) {
        if (connection != null) {
            synchronized (pool) {
                // 释放其实就是归还进数据库连接池中
                pool.addLast(connection);
                // 在通知的时候必须确保已经获取了对象的锁，当归还了数据库连接，通知其他消费者
                pool.notifyAll();
            }
        }
    }

    /**
     * 获取连接
     *
     * @param millis 超时时间
     * @return 连接对象
     */
    public Connection fetchConnection(long millis) throws InterruptedException {
        synchronized (pool) {
            // 完全超时
            if (millis < 0) {
                // 等待
                while (pool.isEmpty()) {
                    pool.wait();
                }
                // 返回第一个
                return pool.removeFirst();
            } else {
                // 等待超时
                long future = System.currentTimeMillis() + millis;
                long remaining = millis;
                while (pool.isEmpty() && remaining > 0) {
                    pool.wait(remaining);
                    remaining = future - System.currentTimeMillis();
                }
                Connection result = null;
                // 双重校验
                if (!pool.isEmpty()) {
                    result = pool.removeFirst();
                }

                return result;
            }
        }
    }
}
