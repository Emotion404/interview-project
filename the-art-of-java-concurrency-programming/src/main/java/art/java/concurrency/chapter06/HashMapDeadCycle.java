package art.java.concurrency.chapter06;


import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * Description: hash map 引发死循环问题
 */
public class HashMapDeadCycle {
    static Map<String, String> map = new HashMap<>(2);

    public static void main(String[] args) throws InterruptedException {

        Thread t = new Thread(() -> {
            for (int i = 0; i < 1000; i++) {
                new Thread(() -> {
                    map.put(UUID.randomUUID().toString(), "");
                }, "FTF" + i).start();
            }
        }, "FTF");

        t.start();
        t.join();
    }

}
