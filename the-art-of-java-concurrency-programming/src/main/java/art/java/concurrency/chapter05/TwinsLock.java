package art.java.concurrency.chapter05;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.AbstractQueuedSynchronizer;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * Description: 自定义同步组件，该组件能够保持在同一时间最多有两个线程，其余的将被阻塞
 */
public class TwinsLock implements Lock {

    private static final Sync sync = new Sync(2);

    private static final class Sync extends AbstractQueuedSynchronizer {
        Sync(int count) {
            if (count < 0) {
                throw new IllegalArgumentException("count must larger than zero.");
            }

            setState(count);
        }

        @Override
        protected int tryAcquireShared(int reduceCount) {
            for (; ; ) {
                System.out.println("线程：" + Thread.currentThread().getName() + "正在抢占资源。");
                int current = getState();
                int newCount = current - reduceCount;
                if (newCount < 0 || compareAndSetState(current, newCount)) {
                    if (newCount > 0) {
                        System.out.println("当前newCount: " + newCount + "，线程：" + Thread.currentThread().getName() + "已经抢占到资源。");
                    } else {
                        System.out.println("newCount: " + newCount + "，即将进入doAcquireShared，当前线程为：" + Thread.currentThread().getName());
                    }

                    return newCount;

                }
            }
        }

        @Override
        protected boolean tryReleaseShared(int reduceCount) {
            for (; ; ) {
                int current = getState();
                int newCount = current + reduceCount;
                if (compareAndSetState(current, newCount)) {
                    System.out.println("线程：" + Thread.currentThread().getName() + " 已经释放同步资源");
                    return true;
                }
            }
        }
    }

    @Override
    public void lock() {
        sync.acquireShared(1);
    }

    @Override
    public void lockInterruptibly() throws InterruptedException {

    }

    @Override
    public boolean tryLock() {
        return false;
    }

    @Override
    public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
        return false;
    }

    @Override
    public void unlock() {
        sync.releaseShared(1);
    }

    @Override
    public Condition newCondition() {
        return null;
    }
}
