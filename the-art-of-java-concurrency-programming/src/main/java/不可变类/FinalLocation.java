package 不可变类;

import lombok.Data;

/**
 * 不可变类
 *
 * @author zhenglian
 * <p>
 * 那么怎么将一个类改造为不可变类呢？所谓的不可变类是指一个对象一经创建就不再改变。
 * <p>
 * 在我们车辆管理系统中来说就是Location类一旦创建就不能变了，不能改变X的值，也不能改变Y的值。
 * <p>
 * <p>
 * <p>
 * 说到这就有点意思了，如果Location类中的X的值不能变，Y的值也不能变，那么我们是不是可以使用Java的关键字final来修饰这两个字段，通过Java语言的语法特性来保证这两个字段的不可变
 * <p>
 * 为了杜绝这子类可以改变它的方法行为，我们需要将Location类设计为不可继承的，通过final修饰符修饰即可。
 */
@Data
public final class FinalLocation {

    private final double x;

    private final double y;

    public FinalLocation(double x, double y) {
        this.x = x;
        this.y = y;
    }
}
