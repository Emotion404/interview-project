package 不可变类;

import lombok.Data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author zhenglian
 * 防御性复制
 */
@Data
public final class 防御性复制 {

    private final List<Integer> data = new ArrayList<Integer>();

    public 防御性复制() {
        data.add(1);
        data.add(2);
        data.add(3);
    }


    public Collection<Integer> getData() {
        // return data;
        return Collections.unmodifiableCollection(new ArrayList<Integer>(data));
    }

    public static void main(String[] args) {
        防御性复制 test = new 防御性复制();
        // 返回的是一个引用，指向的对象和 `防御性复制` 类中的data指向的对象是同一个，
        // 这样就会导致DefensiveReplicaDemo类中的data数据内容改变为1，2，3，4
        Collection<Integer> data = test.getData();
        System.out.println(data);
        data.add(4);


    }

}
