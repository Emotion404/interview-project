package 不可变类;

import lombok.Data;

/**
 * @author zhenglian
 */
@Data
public class Location {

    private double x;

    private double y;

    public void setXY(double x, double y) {
        this.x = x;
        this.y = y;
    }
}
