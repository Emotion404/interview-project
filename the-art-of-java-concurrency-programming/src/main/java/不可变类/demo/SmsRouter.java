package 不可变类.demo;

import java.util.HashMap;
import java.util.Map;

/**
 * @author eric
 */
public class SmsRouter {

    /**
     * 短信网关对象， 使用volatile修饰来保证其他线程可见
     */
    private static volatile SmsRouter instance = new SmsRouter();

    /**
     * 短信服务商信息的map， key对应的是优先级
     */
    private final Map<Integer, SmsInfo> smsInfoMap;

    public SmsRouter() {
        this.smsInfoMap = this.loadSmsInfoRouteMapFromDb();
    }

    public static SmsRouter getInstance() {
        return instance;
    }

    public Map<Integer, SmsInfo> getSmsInfoMap() {
        return smsInfoMap;
    }

    /**
     * 从数据库加载短信服务商信息
     *
     * @return 短信中心的路由信息
     */
    private Map<Integer, SmsInfo> loadSmsInfoRouteMapFromDb() {
        // 初始化， 模拟从数据库中加载
        Map<Integer, SmsInfo> routeMap = new HashMap<>();

        routeMap.put(1, new SmsInfo("https://www.aliyun.com", 180L));
        routeMap.put(2, new SmsInfo("https://cloud.tencent.com", 180L));
        routeMap.put(3, new SmsInfo("https://cloud.baidu.com", 180L));

        return routeMap;
    }


    public static void main(String[] args) {
        Map<Integer, SmsInfo> smsInfoRouteMap = SmsRouter.getInstance().getSmsInfoMap();
        SmsInfo smsInfo = smsInfoRouteMap.get(3);
        // 将服务商排名为3的服务商改为另外一个服务商。
        smsInfo.setUrl("http://www.jiguang.cn");
        // 但是这里有一个问题，因为这里设置url和设置maxSizeInBytes并不是一个原子操作，可能出现其中一个线程刚刚设置了URL，另一个线程过来读取服务商排名为3的服务商的场景，
        // 这样读取排名为3的服务商得到的一个结果就是是：SmsInfo(url = "https://www.jiguang.cn", 182)。
        smsInfo.setMaxSizeInBytes(181L);


    }

}
