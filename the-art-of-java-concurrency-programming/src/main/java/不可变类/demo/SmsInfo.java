package 不可变类.demo;

import lombok.Data;

/**
 * @author eric
 */
@Data
public class SmsInfo {

    /**
     * 短信服务商请求url
     */
    private String url;

    /**
     * 短信内容最多多少字节
     */
    private long maxSizeInBytes;

    public SmsInfo(String url, long maxSizeInBytes) {
        this.url = url;
        this.maxSizeInBytes = maxSizeInBytes;
    }
}
