package 不可变类;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author zhenglian
 */
public class CarLocationTracker {

    /**
     * 车辆编码对应的地理位置
     */
    private Map<String, Location> locationMap = new ConcurrentHashMap<String, Location>();

    /**
     * 更新车辆位置
     *
     * @param carCode 车辆编号
     * @param x       x
     * @param y       y
     */
    public void updateLocation(String carCode, double x, double y) {
        locationMap.get(carCode).setXY(x, y);
    }


    /**
     * @param carCode 汽车编号
     * @return 位置
     */
    public Location getLocation(String carCode) {
        return locationMap.get(carCode);
    }
}


/**
 * 当车辆的位置信息发生变更的时候，我们可以调用updateLocation方法来更新车辆的位置，另外也可以通过调用getLocation方法来获取车辆的信息。
 * 但Location类的setXY方法不是一个线程安全的方法
 * 一开始某辆车的位置信息为x=1.0 y=1.0，接着线程1调用updateLocation方法来更新位置信息为x = 2.0，y = 2.0 ，这时线程1只来得及更新了x的值，y的值还没有更新，好巧不巧，线程2也来读取车辆的位置信息，此时它得到的结果是 x =2.0，y = 1.0。
 * 这可是这个车根本不曾到达过的“诗和远方”。
 * <p>
 * <p>
 * <p>
 * 为了确保车辆信息的更新具备线程安全的特性，我们可以将位置信息类改造为不可变类，如果车辆的位置信息发生变化，咱们通过替换整个Location对象来实现，而不是通过setXY方法来实现。
 */
