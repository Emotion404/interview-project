package 不可变类.retrofit;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zhenglian
 */
public final class SmsInfo {

    /**
     * 服务商编号
     */
    @Getter
    @Setter
    private final Long id;

    /**
     * 短信服务商请求url
     */
    @Getter
    @Setter
    private final String url;

    /**
     * 短信内容最多多少字节
     */
    @Getter
    @Setter
    private final Long maxSizeInBytes;

    public SmsInfo(Long id, String url, Long maxSizeInBytes) {
        this.id = id;
        this.url = url;
        this.maxSizeInBytes = maxSizeInBytes;
    }

    public SmsInfo(SmsInfo smsInfo) {
        this.id = smsInfo.getId();
        this.url = smsInfo.getUrl();
        this.maxSizeInBytes = smsInfo.getMaxSizeInBytes();
    }

}
