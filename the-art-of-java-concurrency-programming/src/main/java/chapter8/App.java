package chapter8;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author eric
 * 测试
 */
public class App {

    public static void main(String[] args) throws InterruptedException {
        GuardedQueue guardedQueue = new GuardedQueue();

        ExecutorService executorService = Executors.newFixedThreadPool(3);
        executorService.execute(() -> {
                    // 线程1获取数据， 此时 队列中数据是空， 当前线程等待
                    System.out.println(guardedQueue.get());
                }
        );


        Thread.sleep(2000);
        executorService.execute(() -> {
                    // 另一个线程塞入数据， 然后通知所有等待的线程，线程1被唤醒，获取数据
                    guardedQueue.put(20);
                }
        );

        executorService.shutdown();
        executorService.awaitTermination(30, TimeUnit.SECONDS);
    }
}
