package chapter14_1;

/**
 * @author eric
 * Description: 线程终止标志位
 */
public interface Termination {

    /**
     * 请求终止线程
     */
    void terminate();
}
