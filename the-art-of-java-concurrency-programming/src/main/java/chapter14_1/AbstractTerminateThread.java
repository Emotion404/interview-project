package chapter14_1;

/**
 * @author eric
 * Description： 终止线程设置标志位的线程
 */
public abstract class AbstractTerminateThread extends Thread implements Termination {

    /**
     * 线程共享停止的标志实例对象
     */
    public final TerminationToken terminationToken;

    public AbstractTerminateThread() {
        this(new TerminationToken());
    }

    /**
     * 终止指定的线程
     */
    @Override
    public void run() {
        Exception ex = null;
        try {
            // 判断中断标志位和上报的任务数
            System.out.println("告警线程执行，此时中断标志位：" + terminationToken.isToShutDown() + ", 未完成的任务数量：" + terminationToken.reservations.get());
            for (; ; ) {
                if (terminationToken.isToShutDown() && terminationToken.reservations.get() <= 0) {
                    // 已经中断，并且没有未完成的任务
                    System.out.println("中断标志为true，未完成任务为0，告警线程退出");
                    break;
                }

                // 执行具体的业务逻辑
                doRun();
            }
        } catch (Exception e) {
            ex = e;
            if (e instanceof InterruptedException) {
                // 中断线程响应退出
                System.out.println("中断响应：" + e);
            }
        } finally {
            try {
                System.out.println("告警线程停止，回调终止后的清理工作");
                doCleanup(ex);
            } finally {
                // 通知terminationToken管理的所有线程实例进行退出
                System.out.println("标志实例对象中一个线程终止，通知其他线程终止");
                terminationToken.noytifyAllThreadTermination(this);
            }
        }
    }

    /**
     * 执行终止线程的逻辑 留个子类去具体的实现
     */
    protected void doTerminate() {

    }

    /**
     * @param terminationToken 线程终止标志
     */
    public AbstractTerminateThread(TerminationToken terminationToken) {
        this.terminationToken = terminationToken;
        System.out.println("注册告警线程到线程停止的标志实现对象队列中");
        terminationToken.register(this);
    }

    /**
     * 请求终止线程
     */
    @Override
    public void terminate() {
        // 设置线程终止标志位
        System.out.println("设置中断标志对象为中断状态");
        terminationToken.setToShutDown(true);

        try {
            // 终止线程之前要做的事
            doTerminate();
        } finally {
            // 如果没有等待的线程 则强制终止线程
            if (terminationToken.reservations.get() <= 0) {
                super.interrupt();
            }
        }
    }

    /**
     * 留给子类去实现，完成线程终止后的一些清理动作
     *
     * @param ex
     */
    protected void doCleanup(Exception ex) {

    }

    /**
     * 留给子类去实现具体的线程业务逻辑
     */
    protected abstract void doRun() throws InterruptedException;
}
