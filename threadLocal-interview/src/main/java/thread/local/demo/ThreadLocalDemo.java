package thread.local.demo;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author eric
 * threadlocal 复习
 */
public class ThreadLocalDemo implements Runnable {
    private static AtomicInteger counter = new AtomicInteger();
    private static ThreadLocal<String> threadLocalInfo = new ThreadLocal<String>() {
        @Override
        protected String initialValue() {
            return "[" + Thread.currentThread().getName() + "," + counter.getAndIncrement() + "]";
        }
    };

    public static void main(String[] args) {
        Thread thread1 = new Thread(new ThreadLocalDemo());
        Thread thread2 = new Thread(new ThreadLocalDemo());

        thread1.start();
        thread2.start();

        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        System.out.println("threadInfo value in main:" + threadLocalInfo.get());
    }


    public void run() {
        System.out.println("threadInfo value:" + threadLocalInfo.get());
    }
}
