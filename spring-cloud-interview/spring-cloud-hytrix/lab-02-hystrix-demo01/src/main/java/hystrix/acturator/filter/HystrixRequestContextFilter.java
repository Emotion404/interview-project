package hystrix.acturator.filter;

import com.netflix.hystrix.strategy.concurrency.HystrixRequestContext;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

@Component
@WebFilter(urlPatterns = "/")
public class HystrixRequestContextFilter implements Filter {

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        // 初始化 HystrixRequestContext
//        HystrixRequestContext context = HystrixRequestContext.initializeContext();
        // 继续过滤器
//        try {
//            chain.doFilter(request, response);
//        } finally {
//            // 销毁 HystrixRequestContext
//            context.close();
//        }

        /**
         * Hystrix 支持在同一个 HystrixRequestContext 上下文中，提供请求合并的功能，将一段时间的相同类型的请求，延迟合并请求服务提供者的批量 API 接口，以减少请求次数，减少服务压力。
         * Hystrix 支持在同一个 HystrixRequestContext 上下文中，提供缓存的功能，以提升高并发场景下的性能，同时也带来相同缓存键返回相同结果的特性。
         **/
        try(HystrixRequestContext context = HystrixRequestContext.initializeContext();){
            chain.doFilter(request, response);
        }
    }

}
