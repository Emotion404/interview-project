package hystrix.acturator.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.cache.annotation.CacheRemove;
import com.netflix.hystrix.contrib.javanica.cache.annotation.CacheResult;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class CacheDemoService {

    private Logger logger = LoggerFactory.getLogger(CacheDemoService.class);

    @Autowired
    private RestTemplate restTemplate;

    /**
     * 使用hystrix 指令
     * 为了提高高并发，将接口的值缓存，CacheResult key为genGetUserCacheKey()生成结果， value为接口返回值
     * @param id
     * @return
     */
    @HystrixCommand
    @CacheResult(cacheKeyMethod = "genGetUserCacheKey")
    public String getUser(Integer id) {
        logger.info("[getUser][准备调用 user-service 获取用户({})详情]", id);
        return restTemplate.getForEntity("http://127.0.0.1:18080/user/get?id=" + id, String.class).getBody();
    }

    /**
     * 使用hystrix 指令
     * 删除缓存，commandKey(Hystrix Command 键 默认为注解方法名)
     * @param id
     */
    @HystrixCommand
    @CacheRemove(commandKey = "getUser", cacheKeyMethod = "genGetUserCacheKey")
    public void updateUser(Integer id) {
        logger.info("[updateUser][更新用户({})详情]", id);
    }

    public String getUserFallback(Integer id, Throwable throwable) {
        logger.info("[getUserFallback][id({}) exception({})]", id, ExceptionUtils.getRootCauseMessage(throwable));
        return "mock:User:" + id;
    }

    public String genGetUserCacheKey(Integer id) {
        return "USER_" + id;
    }

}
