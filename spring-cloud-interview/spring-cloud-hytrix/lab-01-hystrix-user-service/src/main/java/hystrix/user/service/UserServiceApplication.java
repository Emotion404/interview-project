package hystrix.user.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@SpringBootApplication
public class UserServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserServiceApplication.class, args);
    }


    @RestController
    @RequestMapping("/user")
    public class UserController {

        @GetMapping("/get")
        public String get(@RequestParam("id") Integer id) {
            return "User:" + id;
        }

        @GetMapping("/batch_get")
        public List<String> batchGet(@RequestParam("ids") List<Integer> ids) {
            return ids.stream().map(id -> "User:" + id).collect(Collectors.toList());
        }

    }
}
