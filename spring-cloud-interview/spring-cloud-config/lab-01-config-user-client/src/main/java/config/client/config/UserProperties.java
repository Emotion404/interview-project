package config.client.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

/**
 * @author zhenglian
 *
 */
@Component
@ConfigurationProperties(prefix = "user")
@Data
public class UserProperties {

    private String name;

    private String age;
}
