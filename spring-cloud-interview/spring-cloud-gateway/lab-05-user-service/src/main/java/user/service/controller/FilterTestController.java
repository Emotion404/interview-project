package user.service.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zheng'lian
 * 测试过滤器 filter
 */
@RestController
@RequestMapping("/filter")
public class FilterTestController {

    @GetMapping("add-request-parameter")
    public String addResParam(String foo) {
        return "Get from request parameter:" + foo;
    }

}
