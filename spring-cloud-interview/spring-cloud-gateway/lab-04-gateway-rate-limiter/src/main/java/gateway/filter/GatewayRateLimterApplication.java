package gateway.filter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GatewayRateLimterApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayRateLimterApplication.class, args);
    }
}
