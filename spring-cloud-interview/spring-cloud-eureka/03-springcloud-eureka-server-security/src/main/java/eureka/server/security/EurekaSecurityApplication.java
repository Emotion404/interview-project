package eureka.server.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
// 说明自己是服务端
@EnableEurekaServer
public class EurekaSecurityApplication {

    public static void main(String[] args) {
        SpringApplication.run(EurekaSecurityApplication.class, args);
    }
}
