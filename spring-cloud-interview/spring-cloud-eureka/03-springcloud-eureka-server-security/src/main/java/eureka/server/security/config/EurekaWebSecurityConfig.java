package eureka.server.security.config;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.stereotype.Component;

@Component
public class EurekaWebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // 设置 Eureka-Server 提供的 /eureka/** 无需传递 CSRF Token
        http.csrf().ignoringAntMatchers("eureka/**");
        super.configure(http);
    }
}
