package chapter10;

import lombok.Data;

/**
 * @author <a href="mailto:little@163.com">little</a>
 * version: 1.0
 * Description:报警信息
 **/
@Data
public class AlarmInfo {

    /**
     * 楼号
     */
    private Integer no;

    /**
     * 几单元
     */
    private Integer unit;

    /**
     * 几零几
     */
    private String  roomNumber;

    /**
     * 报警类型
     */
    private Integer alarmType;

    public AlarmInfo(Integer no, Integer unit, String roomNumber, Integer alarmType) {
        this.no = no;
        this.unit = unit;
        this.roomNumber = roomNumber;
        this.alarmType = alarmType;
    }

    @Override
    public String toString() {
        return "AlarmInfo{" + "no=" + no + ", unit=" + unit + ", roomNumber='" + roomNumber + '\'' + ", alarmType=" + alarmType + '}';
    }
}
