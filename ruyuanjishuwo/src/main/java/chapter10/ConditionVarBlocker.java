package chapter10;

import java.util.concurrent.Callable;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author eric
 * Description：使用jdk 的condition 来实现Blocker
 */
public class ConditionVarBlocker implements Blocker {

    /**
     * local
     */
    private final Lock lock;

    /**
     * 条件变量
     */
    private final Condition condition;

    /**
     * 是否允许获取当前blocker的锁
     */
    private final boolean allowAccess2Lock;

    public ConditionVarBlocker(Lock lock, Condition condition, boolean allowAccess2Lock) {
        this.lock = lock;
        this.condition = condition;
        this.allowAccess2Lock = allowAccess2Lock;
    }

    public ConditionVarBlocker(boolean allowAccess2Lock) {
        this(new ReentrantLock(), allowAccess2Lock);
    }

    public ConditionVarBlocker(Lock lock, boolean allowAccess2Lock) {
        this.lock = lock;
        this.condition = lock.newCondition();
        this.allowAccess2Lock = allowAccess2Lock;
    }

    @Override
    public <V> V callWithGuard(GuardedAction<V> guardedAction) throws Exception {
        lock.lockInterruptibly();
        try {
            final Predicate predicate = guardedAction.predicate;
            // 条件不满足
            while (!predicate.evaluate()) {
                System.out.println("alarm connecting alarm system，thread wait");
                // 等待
                condition.await();
            }
            // 条件满足，执行目标内容
            System.out.println("alarm connected execute call");
            return guardedAction.call();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void signalAfter(Callable<Boolean> stateOperation) throws Exception {
        lock.lockInterruptibly();
        try {
            if (stateOperation.call()) {
                // 条件满足唤醒
                System.out.println("alarm connected ,signal thread ");
                condition.signal();
            }
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void signal() throws Exception {
        lock.lockInterruptibly();
        try {
            condition.signal();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void broadcastAfter(Callable<Boolean> stateOperation) throws Exception {
        lock.lockInterruptibly();
        try {
            if (stateOperation.call()) {
                // 条件满足唤醒所有等待线程
                condition.signalAll();
            }
        } finally {
            lock.unlock();
        }
    }

    /**
     * 获取锁
     *
     * @return 结果
     */
    public Lock getLock() {
        if (allowAccess2Lock) {
            return this.lock;
        }
        throw new IllegalStateException("access to the lock disallowed");
    }
}
