package chapter11;

/**
 * @author eric
 * 中断顶层接口
 */
public interface Termination {

    /**
     * 请求终止线程
     */
    void terminate();
}
