package docker.interview;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DockerController {

    public static void main(String[] args) {
        SpringApplication.run(DockerController.class, args);
    }
}
