package spring.security.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 【认证】你要登机，你需要出示你的 passport 和 ticket，passport 是为了证明你张三确实是你张三，这就是 authentication。
 * 【授权】而机票是为了证明你张三确实买了票可以上飞机，这就是 authorization。
 * 以论坛举例子：
 *
 * 【认证】你要登录论坛，输入用户名张三，密码 1234，密码正确，证明你张三确实是张三，这就是 authentication。
 * 【授权】再一 check 用户张三是个版主，所以有权限加精删别人帖，这就是 authorization 。
 *
 * Spring Security 是一个框架，侧重于为 Java 应用程序提供身份验证和授权。
 * 与所有 Spring 项目一样，Spring 安全性的真正强大之处，在于它很容易扩展以满足定制需求。
 */
@SpringBootApplication
public class SecurityDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurityDemoApplication.class, args);
    }
}
