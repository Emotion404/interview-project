package 责任链模式;

import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import 行为型模式.责任链模式.demo01.*;

import java.text.ParseException;
import java.util.Date;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * @description: 责任链审批测试
 * <p>
 * 涉及的设计原则有： 单一职责、开闭原则
 * 单一职责： 规定每个类都应该有一个单一的功能，并且该功能应该由这个类完全封装起来。所有它的（这个类的）服务都应该严密的和该功能平行（功能平行，意味着没有依赖）。
 * 开闭原则： 规定 软件中的对象（类，模块，函数等等）应该对于扩展是开放的，但是对于修改是封闭的 ，这意味着一个实体是允许在不改变它的源代码的前提下变更它的行为
 */
@Slf4j
public class Demo01Test {

    @Test
    public void test_AuthLink() throws ParseException {
        AuthLink authLink = new Level3AuthLink("1000013", "王工")
                .appendNext(new Level2AuthLink("1000012", "张经理")
                        .appendNext(new Level1AuthLink("1000011", "段总")));

        log.info("测试结果： {}", JSONUtil.toJsonStr(authLink.doAuth("⼩傅哥",
                "1000998004813441", new Date())));

        // 模拟三级负责⼈审批
        AuthService.auth("1000013", "1000998004813441");
        log.info("测试结果： {}", "模拟三级负责⼈审批，王工");
        log.info("测试结果： {}", JSONUtil.toJsonStr(authLink.doAuth("⼩傅哥",
                "1000998004813441", new Date())));

        // 模拟 二级负责⼈审批
        AuthService.auth("1000012", "1000998004813441");
        log.info("测试结果： {}", "模拟二级负责⼈人审批，张经理");
        log.info("测试结果： {}", JSONUtil.toJsonStr(authLink.doAuth("⼩傅哥",
                "1000998004813441", new Date())));

        // 模拟 一级负责⼈人审批
        AuthService.auth("1000011", "1000998004813441");
        log.info("测试结果： {}", "模拟一级负责人审批，段总");
        log.info("测试结果： {}", JSONUtil.toJsonStr(authLink.doAuth("⼩傅哥",
                "1000998004813441", new Date())));
    }
}
