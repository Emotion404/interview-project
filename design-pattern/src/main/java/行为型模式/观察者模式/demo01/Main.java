package 行为型模式.观察者模式.demo01;

import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * @description:
 */
@Slf4j
public class Main {
    public static void main(String[] args) {
        LotteryService lotteryService = new LotteryServiceImpl();
        LotteryResult result = lotteryService.draw("2765789109876");
        log.info("测试结果： {}", result.toString());

    }
}
