package 行为型模式.观察者模式.demo01;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * @description:
 */
public interface EventListener {

    void doEvent(LotteryResult lotteryResult);
}
