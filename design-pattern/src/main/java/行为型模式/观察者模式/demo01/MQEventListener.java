package 行为型模式.观察者模式.demo01;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MQEventListener implements EventListener {
    private Logger logger = LoggerFactory.getLogger(MQEventListener.class);

    @Override
    public void doEvent(LotteryResult result) {
        logger.info("记录⽤用户 {} 摇号结果(MQ)： {}", result.getuId(),
                result.getMsg());
    }
}