package 行为型模式.观察者模式.demo01;

import java.util.Date;
import java.util.UUID;

public class LotteryServiceImpl extends LotteryService {


    @Override
    protected LotteryResult doDraw(String uId) {
// 摇号
        String lottery = "用户【" + uId + "】摇到号码【" + UUID.randomUUID().toString() + "】";
// 结果
        return new LotteryResult(uId, lottery, new Date());
    }
}