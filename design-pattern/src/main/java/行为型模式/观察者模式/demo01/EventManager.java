package 行为型模式.观察者模式.demo01;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * @description:
 */
public class EventManager {

    Map<Enum<EventType>, List<EventListener>> listener = new HashMap<>();

    public EventManager() {
        this.listener.put(EventType.MQ, new ArrayList<>());
        this.listener.put(EventType.MSG, new ArrayList<>());
    }

    public enum EventType {
        MSG, MQ;
    }

    /**
     * 订阅
     *
     * @param eventType
     * @param eventListener
     */
    public void subscribe(Enum<EventType> eventType, EventListener eventListener) {
        List<EventListener> users = listener.get(eventType);
        users.add(eventListener);
    }


    /**
     * 取消订阅
     *
     * @param eventType
     * @param eventListener
     */
    public void unSubscribe(Enum<EventType> eventType, EventListener eventListener) {
        List<EventListener> users = listener.get(eventType);
        users.remove(eventListener);
    }

    /**
     * 通知
     *
     * @param eventType
     * @param result
     */
    public void notify(Enum<EventType> eventType, LotteryResult result) {
        List<EventListener> users = listener.get(eventType);

        for (EventListener listener : users) {
            listener.doEvent(result);
        }
    }
}
