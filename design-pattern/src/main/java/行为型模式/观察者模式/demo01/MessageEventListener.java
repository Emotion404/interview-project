package 行为型模式.观察者模式.demo01;

import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * @description:
 */
@Slf4j
public class MessageEventListener implements EventListener {
    @Override
    public void doEvent(LotteryResult lotteryResult) {
        log.info("给⽤用户 {} 发送短信通知(短信)： {}", lotteryResult.getuId(), lotteryResult.getMsg());
    }
}
