package 行为型模式.责任链模式.demo01;

import lombok.extern.slf4j.Slf4j;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * @description: 链路抽象定义
 */
@Slf4j
public abstract class AuthLink {

    protected SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    protected String levelUserId;

    protected String levelUserName;

    private AuthLink next;

    public AuthLink(String levelUserId, String levelUserName) {
        this.levelUserId = levelUserId;
        this.levelUserName = levelUserName;
    }

    public AuthLink next() {
        return next;
    }

    /**
     * 添加一个审批节点
     *
     * @param next 下一审批节点
     * @return 返回审批链路
     */
    public AuthLink appendNext(AuthLink next) {
        this.next = next;
        return this;
    }

    /**
     * 审批操作
     *
     * @param uId      发起人
     * @param orderId  审批流程业务id
     * @param authDate 审批日期
     * @return 审批结果
     */
    public abstract AuthInfo doAuth(String uId, String orderId, Date authDate);
}
