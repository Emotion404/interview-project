package 行为型模式.责任链模式.demo01;

import lombok.Getter;
import lombok.Setter;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * @description: 责任链中返回对象定义
 */
public class AuthInfo {

    @Getter
    @Setter
    private String code;

    @Getter
    @Setter
    private String info = "";

    public AuthInfo(String code, String... infos) {
        this.code = code;
        for (String info : infos) {
            this.info = this.info.concat(info);
        }
    }
}
