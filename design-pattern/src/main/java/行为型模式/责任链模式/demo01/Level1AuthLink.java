package 行为型模式.责任链模式.demo01;

import java.util.Date;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * @description: 一级审批
 */
public class Level1AuthLink extends AuthLink {

    public Level1AuthLink(String levelUserId, String levelUserName) {
        super(levelUserId, levelUserName);
    }

    @Override
    public AuthInfo doAuth(String uId, String orderId, Date authDate) {
        Date date = AuthService.queryAuthInfo(levelUserId, orderId);

        if (null == date) {
            return new AuthInfo("0001", "单号: ", orderId, " 待一级审批负责人 ", levelUserName);
        }

        AuthLink next = super.next();

        if (null == next) {
            return new AuthInfo("0000", "单号: ", orderId, " 一级审批完成负责人 ",
                    " 时间:", simpleDateFormat.format(date), " 审批人： ", levelUserName);
        }

        return next.doAuth(uId, orderId, authDate);
    }
}
