package 行为型模式.责任链模式.demo01;

import java.text.ParseException;
import java.util.Date;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * @description: 二级审批
 */
public class Level2AuthLink extends AuthLink {
    private Date beginDate = simpleDateFormat.parse("2021-12-01 00:00:00");
    private Date endDate = simpleDateFormat.parse("2021-12-25 23:59:59");

    public Level2AuthLink(String levelUserId, String levelUserName) throws ParseException, ParseException {
        super(levelUserId, levelUserName);
    }

    @Override
    public AuthInfo doAuth(String uId, String orderId, Date authDate) {
        Date date = AuthService.queryAuthInfo(levelUserId, orderId);
        if (null == date) {
            return new AuthInfo("0001", "单号： ", orderId, " 状态：待⼆二级审批负责人", levelUserName);
        }
        AuthLink next = super.next();
        if (null == next) {
            return new AuthInfo("0000", "单号： ", orderId, " 状态：⼆二级审批完成负责人", " 时间： ",
                    simpleDateFormat.format(date), " 审批⼈人： ", levelUserName);
        }
        if (authDate.before(beginDate) || authDate.after(endDate)) {
            return new AuthInfo("0000", "单号： ", orderId, " 状态：⼆二级审批完成负责⼈", " 时间： ",
                    simpleDateFormat.format(date), " 审批⼈人： ", levelUserName);
        }

        return next.doAuth(uId, orderId, authDate);
    }
}