package 行为型模式.责任链模式.demo02;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * @description: 责任链模式 - 基础验证接口
 */
public abstract class Middleware {

    private Middleware next;

    /**
     * 链接下级链
     *
     * @param next 下个链
     * @return 整体责任链
     */
    public Middleware linkWith(Middleware next) {
        this.next = next;
        return this;
    }

    /**
     * 具体的子类来实现当前的check方法
     *
     * @param email    email account.
     * @param password password
     * @return success / fail
     */
    public abstract boolean check(String email, String password);

    /**
     * 运行在责任链中的下一个对象，直至到最后一个链
     *
     * @param email    email account.
     * @param password password
     * @return success / fail
     */
    protected boolean checkNext(String email, String password) {
        if (next == null) {
            return true;
        }

        return next.check(email, password);
    }
}
