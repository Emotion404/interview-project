package 行为型模式.命令模式.demo02;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * @description:
 */
public class Main {
    public static void main(String[] args) {
        ICuisine guangDoneCuisine = new GuangDoneCuisine(new GuangdongCook());
        ShandongCuisine shandongCuisine = new ShandongCuisine(new ShandongCook());

        // 点单
        Waiter xiaoEr = new Waiter();
        xiaoEr.order(guangDoneCuisine);
        xiaoEr.order(shandongCuisine);
        // 下单
        xiaoEr.placeOrder();
    }
}
