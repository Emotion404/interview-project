package 行为型模式.命令模式.demo02;

import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * @description:
 */
@Slf4j
public class ShandongCook implements ICook {

    @Override
    public void doCooking() {
        log.info("山东厨师doCooking.....");
    }
}
