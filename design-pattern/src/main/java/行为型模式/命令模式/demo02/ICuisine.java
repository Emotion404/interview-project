package 行为型模式.命令模式.demo02;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * @description: 菜系
 */
public interface ICuisine {

    /**
     * 制作
     */
    void cook();
}
