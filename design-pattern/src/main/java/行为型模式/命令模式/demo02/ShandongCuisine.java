package 行为型模式.命令模式.demo02;

/**
 * @author Eric
 * @description 山东菜系
 */
public class ShandongCuisine implements ICuisine {
    private ICook cook;

    public ShandongCuisine(ICook cook) {
        this.cook = cook;
    }

    @Override
    public void cook() {
        cook.doCooking();
    }
}