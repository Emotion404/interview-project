package 行为型模式.命令模式.demo02;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * @description: 服务员
 */
@Slf4j
public class Waiter {

    List<ICuisine> cuisines = new ArrayList<>();

    public void order(ICuisine cuisine) {
        this.cuisines.addAll(cuisines);
    }

    /**
     * 下单
     */
    public synchronized void placeOrder() {
        for (ICuisine cuisine : cuisines) {
            cuisine.cook();
        }
        cuisines.clear();
    }
}
