package 行为型模式.命令模式.demo02;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * @description: 厨师接口
 */
public interface ICook {

    void doCooking();
}
