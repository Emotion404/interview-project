package 结构型模式.组合模式.demo01.logic;

import 结构型模式.组合模式.demo01.model.vo.TreeNodeLink;

import java.util.List;
import java.util.Map;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * @description: 树节点逻辑过滤器接口 定义了了适配的通用接口，逻辑决策器、获取决策值，让每个提供决策能⼒力的节点都必须
 * 实现此接口，保证统一性
 */
public interface LogicFilter {

    /**
     * 逻辑决策器
     *
     * @param matterValue   决策值
     * @param treeNodeLinks 决策节点
     * @return 下一个节点id
     */
    Long filter(String matterValue, List<TreeNodeLink> treeNodeLinks);

    /**
     * 获取决策值
     *
     * @param decisionMatter 决策物料
     * @return 决策值
     */
    String matterValue(Long treeId, String userId, Map<String, String> decisionMatter);

}
