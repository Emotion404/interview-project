package 结构型模式.组合模式.demo01.logic;

import 结构型模式.组合模式.demo01.model.vo.TreeNodeLink;

import java.util.List;
import java.util.Map;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * @description: 同时定义了了基本的决策⽅方法； 1、 2、 3、 4、 5 ， 等于、小于、⼤于、⼩于等于、⼤于等于 的判断逻辑
 */
public abstract class BaseLogic implements LogicFilter {

    @Override
    public Long filter(String matterValue, List<TreeNodeLink> treeNodeLinks) {
        for (TreeNodeLink nodeLine : treeNodeLinks) {
            if (decisionLogic(matterValue, nodeLine)) {
                return nodeLine.getNodeIdTo();
            }
        }
        return 0L;
    }

    /**
     * 定义了抽象⽅方法，让每个实现接口的类都必须按照规则提供 决策值 ，这个决策值用于做逻辑比对
     *
     * @param treeId         逻辑树id
     * @param userId         用户id
     * @param decisionMatter 决策物料
     * @return
     */
    @Override
    public abstract String matterValue(Long treeId, String userId, Map<String, String> decisionMatter);


    /**
     * 决策逻辑
     *
     * @param matterValue 物料值
     * @param nodeLink    节点链接
     * @return
     */
    private boolean decisionLogic(String matterValue, TreeNodeLink nodeLink) {
        switch (nodeLink.getRuleLimitType()) {
            case 1:
                return matterValue.equals(nodeLink.getRuleLimitValue());
            case 2:
                return Double.parseDouble(matterValue) > Double.parseDouble(nodeLink.getRuleLimitValue());
            case 3:
                return Double.parseDouble(matterValue) < Double.parseDouble(nodeLink.getRuleLimitValue());
            case 4:
                return Double.parseDouble(matterValue) <= Double.parseDouble(nodeLink.getRuleLimitValue());
            case 5:
                return Double.parseDouble(matterValue) >= Double.parseDouble(nodeLink.getRuleLimitValue());
            default:
                return false;
        }
    }

}
