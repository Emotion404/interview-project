package 结构型模式.组合模式.demo01.filter;

import 结构型模式.组合模式.demo01.logic.BaseLogic;

import java.util.Map;

/**
 * @author Eric
 * @description 实际的业务开发可以从数据库、 RPC接⼝口、缓存运算等各种⽅方式获取。
 */
public class UserGenderFilter extends BaseLogic {
    @Override
    public String matterValue(Long treeId, String userId, Map<String, String> decisionMatter) {
        return decisionMatter.get("gender");
    }
}