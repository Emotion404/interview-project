package 结构型模式.组合模式.demo01.engine;

import 结构型模式.组合模式.demo01.filter.UserAgeFilter;
import 结构型模式.组合模式.demo01.filter.UserGenderFilter;
import 结构型模式.组合模式.demo01.logic.LogicFilter;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * @description:
 */
public class EngineConfig {
    
    static Map<String, LogicFilter> logicFilterMap;

    static {
        logicFilterMap = new ConcurrentHashMap<>();
        logicFilterMap.put("userAge", new UserAgeFilter());
        logicFilterMap.put("userGender", new UserGenderFilter());
    }

    public Map<String, LogicFilter> getLogicFilterMap() {
        return logicFilterMap;
    }

    public void setLogicFilterMap(Map<String, LogicFilter> logicFilterMap) {
        EngineConfig.logicFilterMap = logicFilterMap;
    }
}
