package 结构型模式.组合模式.demo01.engine;

import 结构型模式.组合模式.demo01.model.TreeRich;
import 结构型模式.组合模式.demo01.model.vo.EngineResult;
import 结构型模式.组合模式.demo01.model.vo.TreeNode;

import java.util.Map;

/**
 * @author Eric
 */
public class TreeEngineHandle extends EngineBase {
    @Override
    public EngineResult process(Long treeId, String userId, TreeRich treeRich, Map<String, String> decisionMatter) {
        // 决策流程
        TreeNode treeNode = engineDecisionMaker(treeRich, treeId, userId, decisionMatter);
        // 决策结果
        return new EngineResult(userId, treeId, treeNode.getTreeNodeId(),
                treeNode.getNodeValue());
    }
}