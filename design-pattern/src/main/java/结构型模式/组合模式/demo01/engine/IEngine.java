package 结构型模式.组合模式.demo01.engine;

import 结构型模式.组合模式.demo01.model.TreeRich;
import 结构型模式.组合模式.demo01.model.vo.EngineResult;

import java.util.Map;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * @description: 决策引擎接口
 * 对于使⽤用⽅方来说也同样需要定义统一的接口操作，这样的好处非常方便后续拓展出不同类型的决策引擎，也就是可以建造不同的决策工厂。
 */
public interface IEngine {

    EngineResult process(final Long treeId, final String userId, TreeRich treeRich, final Map<String, String> decisionMatter);
}
