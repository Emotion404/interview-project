package 创建型模式.静态工厂模式.demo01;

/**
 * @author eric
 * @date 2021/12/16 22:34
 * @description 工厂模式的定义： 定义一个创建产品对象的工厂接口，将产品对象的实际创建工作推迟到具体的子工厂类中，满足创建型模式下所要求的”创建与使用相分离“的特点
 * 角色：
 * 1.产品： 需要被创建的对象
 * 2.工厂： 创建产品的对象
 * 如果创建的产品不多，只要一个工厂类就可以完成，这种模式叫简单工厂模式。
 * 在简单工厂模式中创建产品的方法通常为”静态“，所以又称简单工厂为静态工厂
 * <p>
 * 简单工厂模式的主要角色如下：
 * 简单工厂（SimpleFactory）：是简单工厂模式的核心，负责实现创建所有实例的内部逻辑。工厂类的创建产品类的方法可以被外界直接调用，创建所需的产品对象。
 * 抽象产品（Product）：是简单工厂创建的所有对象的父类，负责描述所有实例共有的公共接口。
 * 具体产品（ConcreteProduct）：是简单工厂模式的创建目标。
 */
public class Main {

    public static void main(String[] args) {
        Product product = SimpleFactory.makeProduct(1);
        product.show();
    }

    public interface Product {
        void show();
    }

    static class ConcreteProduct1 implements Product {

        @Override
        public void show() {
            System.out.println("创建产品1");
        }
    }

    static class ConcreteProduct2 implements Product {

        @Override
        public void show() {
            System.out.println("创建产品2");
        }
    }

    final class Const {
        static final int PRODUCT_A = 0;
        static final int PRODUCT_B = 1;
        static final int PRODUCT_C = 2;
    }

    static class SimpleFactory {
        public static Product makeProduct(int kind) {
            switch (kind) {
                case Const.PRODUCT_A:
                    return new ConcreteProduct1();
                case Const.PRODUCT_B:
                    return new ConcreteProduct2();
            }
            return null;
        }
    }

}
