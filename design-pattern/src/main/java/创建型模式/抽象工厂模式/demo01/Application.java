package 创建型模式.抽象工厂模式.demo01;

import 创建型模式.抽象工厂模式.demo01.buttons.Button;
import 创建型模式.抽象工厂模式.demo01.checkboxes.Checkbox;
import 创建型模式.抽象工厂模式.demo01.factory.GUIFactory;

/**
 * Factory users don't care which concrete factory they use since they work with
 * factories and products through abstract interfaces.
 * <p>
 * 使用工厂模式的用户并不在乎具体要创建哪个工厂，因为他们通过抽象的接口来执行这些工厂与产品
 *
 * @author Eric
 */
public class Application {
    private Button button;
    private Checkbox checkbox;

    public Application(GUIFactory factory) {
        button = factory.createButton();
        checkbox = factory.createCheckbox();
    }

    public void paint() {
        button.paint();
        checkbox.paint();
    }
}