package 创建型模式.抽象工厂模式.demo01;

import 创建型模式.抽象工厂模式.demo01.factory.GUIFactory;
import 创建型模式.抽象工厂模式.demo01.factory.MacOSFactory;
import 创建型模式.抽象工厂模式.demo01.factory.WindowsFactory;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * @description: demo
 */
public class Demo {

    /**
     * 根据环境变量来选择工厂并创建
     */
    public static Application configurationApp() {
        Application app;
        GUIFactory factory;
        // 获取系统
        String osName = System.getProperty("os.name").toLowerCase();
        if (osName.equals("mac")) {
            factory = new MacOSFactory();
            app = new Application(factory);
        } else {
            factory = new WindowsFactory();
            app = new Application(factory);
        }

        return app;

    }

    public static void main(String[] args) {
        Application app = configurationApp();
        app.paint();
    }

}
