package 创建型模式.抽象工厂模式.demo01.factory;

import 创建型模式.抽象工厂模式.demo01.buttons.Button;
import 创建型模式.抽象工厂模式.demo01.buttons.WindowsButton;
import 创建型模式.抽象工厂模式.demo01.checkboxes.Checkbox;
import 创建型模式.抽象工厂模式.demo01.checkboxes.WindowsCheckbox;

/**
 * Each concrete factory extends basic factory and responsible for creating
 * products of a single variety.
 *
 * @author Eric
 */
public class WindowsFactory implements GUIFactory {


    @Override
    public Button createButton() {
        return new WindowsButton();
    }


    @Override
    public Checkbox createCheckbox() {
        return new WindowsCheckbox();
    }
}