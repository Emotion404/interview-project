package 创建型模式.抽象工厂模式.demo01.checkboxes;

/**
 * Checkboxes is the second product family. It has the same variants as buttons.
 *
 * @author Eric
 */
public interface Checkbox {
    void paint();
}