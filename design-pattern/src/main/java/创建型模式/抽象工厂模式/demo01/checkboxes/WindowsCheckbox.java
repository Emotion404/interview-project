package 创建型模式.抽象工厂模式.demo01.checkboxes;

/**
 * All products families have the same varieties (MacOS/Windows).
 * <p>
 * This is another variant of a checkbox.
 *
 * @author Eric
 */
public class WindowsCheckbox implements Checkbox {


    public void paint() {
        System.out.println("You have created WindowsCheckbox.");
    }
}