package 创建型模式.抽象工厂模式.demo01.buttons;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * @description: 所有系列都有相同的品种
 */
public class WindowsButton implements Button {

    public void paint() {
        System.out.println("You have created WindowsButton.");
    }
}