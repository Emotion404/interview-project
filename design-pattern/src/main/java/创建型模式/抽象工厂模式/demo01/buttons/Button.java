package 创建型模式.抽象工厂模式.demo01.buttons;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * @description: 抽象工厂假设有一些产品分类， 结构化为单独的类层次结构（按钮/复选框）。所有产品的系列都有一个通用的接口
 */
public interface Button {
    /**
     * 绘画
     */
    void paint();

}
