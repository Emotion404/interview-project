package 创建型模式.抽象工厂模式.demo02;

import 创建型模式.抽象工厂模式.demo02.adapter.EGMCacheAdapter;
import 创建型模式.抽象工厂模式.demo02.adapter.IIRCacheAdapter;
import 创建型模式.抽象工厂模式.demo02.classloader.JDKProxy;
import 创建型模式.抽象工厂模式.demo02.redis.service.CacheService;
import 创建型模式.抽象工厂模式.demo02.redis.service.CacheServiceImpl;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * @description: 使用jdk代理测试
 */
public class Main {

    public static void main(String[] args) throws Exception {
        CacheService proxy_EGM = JDKProxy.getProxy(CacheServiceImpl.class, new EGMCacheAdapter());
        proxy_EGM.set("user_name_01", "小傅哥");
        String val01 = proxy_EGM.get("user_name_01");
        System.out.println("测试结果：" + val01);

        CacheService proxy_IIR = JDKProxy.getProxy(CacheServiceImpl.class, new IIRCacheAdapter());
        proxy_IIR.set("user_name_01", "小傅哥");
        String val02 = proxy_IIR.get("user_name_01");
        System.out.println("测试结果：" + val02);
    }

}
