package 创建型模式.抽象工厂模式.demo02.adapter;

import 创建型模式.抽象工厂模式.demo02.redis.matter.IIR;

import java.util.concurrent.TimeUnit;

/**
 * iir
 *
 * @author Eric
 */
public class IIRCacheAdapter implements ICacheAdapter {
    private IIR iir = new IIR();

    @Override
    public String get(String key) {
        return iir.get(key);
    }

    @Override
    public void set(String key, String value) {
        iir.set(key, value);
    }

    @Override
    public void set(String key, String value, long timeout, TimeUnit
            timeUnit) {
        iir.setExpire(key, value, timeout, timeUnit);
    }

    @Override
    public void del(String key) {
        iir.del(key);
    }
}