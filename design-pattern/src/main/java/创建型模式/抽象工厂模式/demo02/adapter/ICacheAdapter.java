package 创建型模式.抽象工厂模式.demo02.adapter;

import java.util.concurrent.TimeUnit;

/**
 * 缓存抽象适配器
 *
 * @author Eric
 */
public interface ICacheAdapter {

    String get(String key);

    void set(String key, String value);

    void set(String key, String value, long timeout, TimeUnit timeUnit);

    void del(String key);

}