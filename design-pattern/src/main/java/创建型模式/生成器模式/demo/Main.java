package 创建型模式.生成器模式.demo;

import 创建型模式.生成器模式.demo.builders.CarBuilder;
import 创建型模式.生成器模式.demo.builders.CarManualBuilder;
import 创建型模式.生成器模式.demo.cars.Car;
import 创建型模式.生成器模式.demo.cars.Manual;
import 创建型模式.生成器模式.demo.director.Director;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * @description: demo
 */
public class Main {

    public static void main(String[] args) {
        Director director = new Director();

        // Director gets the concrete builder object from the client
        // (application code). That's because application knows better which
        // builder to use to get a specific product.
        CarBuilder builder = new CarBuilder();
        director.constructSportsCar(builder);

        // The final product is often retrieved from a builder object, since
        // Director is not aware and not dependent on concrete builders and
        // products.
        Car car = builder.build();
        System.out.println("Car built:\n" + car.getCarType());

        CarManualBuilder manualBuilder = new CarManualBuilder();

        // Director may know several building recipes.
        director.constructSportsCar(manualBuilder);
        Manual carManual = manualBuilder.build();
        System.out.println("\nCar manual built:\n" + carManual.print());
    }

}
