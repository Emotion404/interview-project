package 创建型模式.生成器模式.demo.cars;

/**
 * 汽车类型
 *
 * @author Eric
 */
public enum CarType {
    CITY_CAR, SPORTS_CAR, SUV
}