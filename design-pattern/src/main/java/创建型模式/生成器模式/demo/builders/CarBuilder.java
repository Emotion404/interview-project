package 创建型模式.生成器模式.demo.builders;

import 创建型模式.生成器模式.demo.cars.Car;
import 创建型模式.生成器模式.demo.cars.CarType;
import 创建型模式.生成器模式.demo.components.Engine;
import 创建型模式.生成器模式.demo.components.GPSNavigator;
import 创建型模式.生成器模式.demo.components.Transmission;
import 创建型模式.生成器模式.demo.components.TripComputer;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * @description: 汽车建造器
 */
public class CarBuilder implements Builder {

    private CarType carType;
    private int seats;
    private Engine engine;
    private Transmission transmission;
    private TripComputer tripComputer;
    private GPSNavigator gpsNavigator;

    @Override
    public CarBuilder setCarType(CarType type) {
        this.carType = type;
        return this;
    }

    @Override
    public CarBuilder setSeats(int seats) {
        this.seats = seats;
        return this;
    }

    @Override
    public CarBuilder setEngine(Engine engine) {
        this.engine = engine;
        return this;
    }

    @Override
    public CarBuilder setTransmission(Transmission transmission) {
        this.transmission = transmission;
        return this;
    }

    @Override
    public CarBuilder setTripComputer(TripComputer tripComputer) {
        this.tripComputer = tripComputer;
        return this;
    }

    @Override
    public CarBuilder setGPSNavigator(GPSNavigator gpsNavigator) {
        this.gpsNavigator = gpsNavigator;
        return this;
    }

    public Car build() {
        return new Car(carType, seats, engine, transmission, tripComputer, gpsNavigator);
    }

}
