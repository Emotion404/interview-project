package 创建型模式.单例模式.demo01;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * @description: 单例模式内部静态类
 */
public class Singleton04 {

    private Singleton04() {
    }

    /**
     * 内部静态类在对象初始化时不去加载，在调用的时候才会初始化内部类
     * 好处：
     * 1. 线程安全
     * 2. 懒汉式
     * 3. 延迟加载
     * 4. 看起来牛逼
     */
    private static class SingletonHolder {
        private static Singleton04 instance = new Singleton04();

        public SingletonHolder() {
            System.out.println("SingletonHolder 被初始化......");
        }
    }

    public static Singleton04 getInstance() {
        return SingletonHolder.instance;
    }

    public static void main(String[] args) {
        new Singleton04();
    }
}
