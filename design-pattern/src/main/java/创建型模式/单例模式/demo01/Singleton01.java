package 创建型模式.单例模式.demo01;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * @description: 单例模式基础版
 */
public class Singleton01 {

    private static Singleton01 instance = new Singleton01();

    public Singleton01 getInstance() {

        return instance;
    }
}
