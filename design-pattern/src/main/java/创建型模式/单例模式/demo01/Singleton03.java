package 创建型模式.单例模式.demo01;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * @description: 单例模式枚举版
 */
public enum Singleton03 {

    INSTANCE;

    public void biz() {
        // ....
    }

}
