package 创建型模式.单例模式.demo01;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * @description: 单例模式双重检查锁版
 */
public class Singleton02 {

    private static Singleton02 instance = null;

    private static final Object LOCK_OBJ = new Object();

    public static Singleton02 getInstance() {
        if (instance == null) {
            synchronized (LOCK_OBJ) {
                if (instance == null) {
                    instance = new Singleton02();
                }
            }
        }

        return instance;
    }

}
