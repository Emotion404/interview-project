package 创建型模式.单例模式.demo01;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * @description: 懒汉式
 */
public class Singleton {

    private static Singleton instance = null;

    public Singleton getInstance() {
        // 只在需要用的时候菜创建
        if (instance == null) {
            instance = new Singleton();
        }

        return instance;
    }
}
