package tl.study.jvm;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @date 2022/8/16
 * @description
 */
public class JvmTestUser {

    public void say() {
        System.out.println("hello");
    }

    public static void main(String[] args) {
        new JvmTestUser().say();
    }
}
