package quartz.memery.config;

import org.quartz.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import quartz.memery.job.DemoJob01;
import quartz.memery.job.DemoJob02;

@Configuration
public class ScheduleConfiguration {

    public static class DemoJob01Configuration {

        @Bean
        public JobDetail demo01Job() {
            return JobBuilder.newJob(DemoJob01.class).withIdentity("job-01").storeDurably()// 没有 Trigger 关联的时候任务是否被保留。因为创建 JobDetail 时，还没 Trigger(触发器) 指向它，所以需要设置为 true ，表示保留。
                .build();
        }

        @Bean
        public Trigger demo01JobTrigger() {
            // 设置调度器 5秒一次， 永远重复
            SimpleScheduleBuilder simpleScheduleBuilder = SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(5) // 频率。
                .repeatForever();// 次数
            // 创建触发器对象， 设置job和调度器
            return TriggerBuilder.newTrigger().withIdentity("job-01-trigger").forJob(demo01Job()).withSchedule(simpleScheduleBuilder).build();
        }

    }

    public static class DemoJob02Configuration {

        @Bean
        public JobDetail demoJob02() {
            return JobBuilder.newJob(DemoJob02.class).withIdentity("demoJob02") // 名字为 demoJob02
                .storeDurably() // 没有 Trigger 关联的时候任务是否被保留。因为创建 JobDetail 时，还没 Trigger 指向它，所以需要设置为 true ，表示保留。
                .build();
        }

        @Bean
        public Trigger demoJob02Trigger() {
            //  基于 Quartz Cron 表达式的调度计划的构造器
            CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule("0/10 * * * * ? *");
            // Trigger 构造器
            return TriggerBuilder.newTrigger().forJob(demoJob02()) // 对应 Job 为 demoJob02
                .withIdentity("demoJob02Trigger") // 名字为 demoJob02Trigger
                .withSchedule(scheduleBuilder) // 对应 Schedule 为 scheduleBuilder
                .build();
        }

    }
}
