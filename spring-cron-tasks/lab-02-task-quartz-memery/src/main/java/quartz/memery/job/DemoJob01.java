package quartz.memery.job;

import java.util.concurrent.atomic.AtomicInteger;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

import quartz.memery.service.DemoService;

public class DemoJob01 extends QuartzJobBean {

    private Logger                     logger = LoggerFactory.getLogger(getClass());

    private static final AtomicInteger counts = new AtomicInteger();

    @Autowired
    private DemoService                demoService;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        logger.info("[executeInternal][定时第 ({}) 次执行, 当前线程为{}]", counts.incrementAndGet(), Thread.currentThread().getName());
    }
}
