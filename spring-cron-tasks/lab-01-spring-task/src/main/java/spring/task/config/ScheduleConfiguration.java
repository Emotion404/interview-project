package spring.task.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
// 开启定时任务功能
@EnableScheduling
public class ScheduleConfiguration {
}
