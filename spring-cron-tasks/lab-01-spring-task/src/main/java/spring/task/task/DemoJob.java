package spring.task.task;

import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class DemoJob {
    private Logger              logger = LoggerFactory.getLogger(getClass());

    private final AtomicInteger counts = new AtomicInteger();

    /**
     * 设置每 2 秒执行该方法。
     * @Scheduled 注解，可以添加在一个类上的多个方法上
     * @Scheduled 注解，设置定时任务的执行计划。
     * fixedDelay 属性：固定执行间隔，单位：毫秒。注意，以调用完成时刻为开始计时时间。
     * fixedRate 属性：固定执行间隔，单位：毫秒。注意，以调用开始时刻为开始计时时间
     */
    @Scheduled(fixedRate = 2000)
    public void execute() {
        logger.info("[execute][定时第 ({}) 次执行， 当前线程为{}]", counts.incrementAndGet(), Thread.currentThread().getName());
    }
}
