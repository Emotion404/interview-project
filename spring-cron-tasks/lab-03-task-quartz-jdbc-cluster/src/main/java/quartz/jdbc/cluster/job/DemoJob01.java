package quartz.jdbc.cluster.job;

import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

import quartz.jdbc.cluster.service.DemoService;

/**
 *  Quartz 的 @DisallowConcurrentExecution 注解，保证相同 JobDetail 在多个 JVM 进程中，有且仅有一个节点在执行。
 *  以 Quartz Job 为维度，保证在多个 JVM 进程中，有且仅有一个节点在执行，而是以 JobDetail 为维度。虽然说，绝大多数情况下，我们会保证一个 Job 和 JobDetail 是一一对应。
 *  😈 所以，保证一个 Job 和 JobDetail 是一一对应就对了。
 *  
 *   JobDetail 的唯一标识是 JobKey ，使用 name + group 两个属性。
 *   一般情况下，我们只需要设置 name 即可，而 Quartz 会默认 group = DEFAULT 。
 */
public class DemoJob01 extends QuartzJobBean {

    private Logger      logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private DemoService demoService;

    @Override
    protected void executeInternal(JobExecutionContext context) {
        logger.info("[executeInternal][我开始的执行了, demoService 为 ({})]", demoService);
    }

}