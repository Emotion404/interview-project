package com.eric.middleware.hystrix.valve;

import com.eric.middleware.hystrix.annotation.DoHystrix;
import org.aspectj.lang.ProceedingJoinPoint;

import java.lang.reflect.Method;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * Description:
 */
public interface IValveService {

    /**
     * 放行
     *
     * @param jp        join point
     * @param method    method
     * @param doHystrix doHystrix
     * @param args      args
     * @return results
     */
    Object access(ProceedingJoinPoint jp, Method method, DoHystrix doHystrix, Object[] args) throws Throwable;
}
