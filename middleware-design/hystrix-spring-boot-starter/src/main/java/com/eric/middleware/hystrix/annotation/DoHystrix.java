package com.eric.middleware.hystrix.annotation;

import cn.hutool.core.util.StrUtil;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * Description: 处理熔断注解
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface DoHystrix {

    /**
     * 失败结果
     *
     * @return 失败结果
     */
    String returnJson() default StrUtil.EMPTY;

    /**
     * 超时时间
     *
     * @return 超时时间
     */
    int timeoutValue() default 0;

}
