package com.eric.middleware.whitelist.annotation;

import cn.hutool.core.util.StrUtil;

import java.lang.annotation.*;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * Description: 白名单自定义注解
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
public @interface DoWhiteList {

    String key() default StrUtil.EMPTY;

    String returnJson() default StrUtil.EMPTY;
}
