package com.eric.middleware.whitelist.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * Description: 白名单自动配置类
 * @Configuration，可以算作是一个组件注解，在 SpringBoot 启动时可以进行加载创建出 Bean 文件。因为 @Configuration 注解有一个 @Component 注解
 * @ConditionalOnClass(WhiteListProperties.class)，当 WhiteListProperties 位于当前类路径上，才会实例化一个类。除此之外还有其他属于此系列的常用的注解。
 * @ConditionalOnBean 仅仅在当前上下文中存在某个对象时，才会实例化一个 Bean
 * @ConditionalOnClass 某个 CLASS 位于类路径上，才会实例化一个 Bean
 * @ConditionalOnExpression 当表达式为 true 的时候，才会实例化一个 Bean
 * @ConditionalOnMissingBean 仅仅在当前上下文中不存在某个对象时，才会实例化一个 Bean
 * @ConditionalOnMissingClass 某个 CLASS 类路径上不存在的时候，才会实例化一个 Bean
 */
@Configuration
@ConditionalOnClass(WhiteListProperties.class)
@EnableConfigurationProperties(WhiteListProperties.class)
public class WhiteListAutoConfigure {

    @Bean("whiteListConfig")
    @ConditionalOnMissingBean
    public String whiteListConfig(WhiteListProperties whiteListProperties) {
        return whiteListProperties.getUsers();
    }

}
