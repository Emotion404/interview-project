package com.eric.middleware.whitelist;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * Description:
 */
@SpringBootApplication
public class WhiteListTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(WhiteListTestApplication.class, args);
    }
}
