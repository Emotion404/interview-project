package sentinel.nacos.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Rocket
 */
@SpringBootApplication
public class SentinelNacosApplication {

    public static void main(String[] args) {
        System.setProperty("project.name", "sentinel-nacos");
        SpringApplication.run(SentinelNacosApplication.class, args);
    }
}
