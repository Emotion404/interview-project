package sentinel.nacos.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.alibaba.csp.sentinel.adapter.spring.webmvc.SentinelWebInterceptor;
import com.alibaba.csp.sentinel.adapter.spring.webmvc.config.SentinelWebMvcConfig;

@Configuration
public class SpringWebMvcConfiguration implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        addSentinelWebIntercepter(registry);
    }

    private void addSentinelWebIntercepter(InterceptorRegistry registry) {
        // <1.1> 创建 SentinelWebMvcConfig 对象
        SentinelWebMvcConfig config = new SentinelWebMvcConfig();
        // <1.2> 是否包含请求方法。即基于 URL 创建的资源，是否包含 Method。
        config.setHttpMethodSpecify(true);
        // config.setBlockExceptionHandler(new DefaultBlockExceptionHandler()); // <1.3> 设置 BlockException 处理器。
        // 设置请求来源解析器。用于黑白名单控制功能
        config.setOriginParser(httpServletRequest -> {
            // 从Header中获取请求来源
            String origin = httpServletRequest.getParameter("s-user");
            // 如果为空， 设置一个默认的
            if (StringUtils.isEmpty(origin)) {
                origin = "default";
            }
            return origin;
        });

        // <2> 添加 SentinelWebInterceptor 拦截器
        registry.addInterceptor(new SentinelWebInterceptor(config)).addPathPatterns("/**");
    }
}
