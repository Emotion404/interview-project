package sentinel.nacos.demo.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.csp.sentinel.slots.block.BlockException;

/**
 * 设置全局的异常
 * @author Rocket
 */
@ControllerAdvice(basePackages = "sentinel.nacos.demo.controller")
public class GolbleExceptionHandler {

    @ResponseBody
    @ExceptionHandler(value = BlockException.class)
    public String blockException(BlockException blockException) {
        return "请求次数过多，请点慢一点";
    }
}
