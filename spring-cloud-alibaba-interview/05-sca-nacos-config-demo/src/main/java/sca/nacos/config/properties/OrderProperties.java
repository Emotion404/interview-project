package sca.nacos.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

/**
 * @author Rocket
 */
@Data
@Component
@ConfigurationProperties(prefix = "order")
public class OrderProperties {

    private Integer payTimeoutSeconds;

    private Integer createSeconds;
}
