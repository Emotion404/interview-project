package sca.sentinel.consumer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sca.sentinel.consumer.feign.DemoProviderFeignClient;

@RestController
@RequestMapping("/consumer")
public class ConsumerController {

    @Autowired
    private DemoProviderFeignClient demoProviderFeignClient;

    @GetMapping("/echo")
    public String echo() {
        return demoProviderFeignClient.echo();
    }

}