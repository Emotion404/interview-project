package sca.sentinel.consumer.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import sca.sentinel.consumer.component.DemoProviderFeignClientFallbackFactory;

/**
 * @author Rocket
 */
@FeignClient(name = "sca-sentinel-consumer", url = "http://127.0.0.1:28082", fallbackFactory = DemoProviderFeignClientFallbackFactory.class)
public interface DemoProviderFeignClient {

    @GetMapping("/demo/echo")
    String echo();
}
