package sca.sentinel.consumer.component;

import sca.sentinel.consumer.feign.DemoProviderFeignClient;

/**
 * @author Rocket
 */
public class DemoProviderFeignClientFallback implements DemoProviderFeignClient {

    private Throwable throwable;

    public DemoProviderFeignClientFallback(Throwable throwable) {
        this.throwable = throwable;
    }

    @Override
    public String echo() {
        return "fallback:" + throwable.getClass().getSimpleName();
    }

}