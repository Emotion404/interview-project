package sca.sentinel.provider.config;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.fastjson.JSONObject;

/**
 * @author Rocket
 * @apiNote 全局的sentinel异常
 */
@Component
@ControllerAdvice(basePackages = "sca.sentinel.provider")
public class GlobalExceptionHandler {

    @ResponseBody
    @ExceptionHandler(value = BlockException.class)
    public JSONObject blockExceptionHandler(BlockException blockException) {
        return new JSONObject().fluentPut("code", 1024).fluentPut("msg", "请求被拦截，拦截类型为" + blockException.getClass().getSimpleName());
    }
}
