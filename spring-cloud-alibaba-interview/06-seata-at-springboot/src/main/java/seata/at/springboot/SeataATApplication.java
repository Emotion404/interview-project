package seata.at.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Rocket
 */
@SpringBootApplication
public class SeataATApplication {

    public static void main(String[] args) {
        SpringApplication.run(SeataATApplication.class, args);
    }
}
