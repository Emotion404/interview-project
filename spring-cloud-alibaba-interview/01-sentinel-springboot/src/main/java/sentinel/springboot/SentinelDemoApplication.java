package sentinel.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Rocket
 */
@SpringBootApplication
public class SentinelDemoApplication {

    public static void main(String[] args) {
        // <X> 设置系统属性 project.name，提供给 Sentinel 读取
        System.setProperty("project.name", "demo-application");

        SpringApplication.run(SentinelDemoApplication.class, args);
    }
}
