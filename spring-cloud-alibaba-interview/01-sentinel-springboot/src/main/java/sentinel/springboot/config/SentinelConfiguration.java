package sentinel.springboot.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.alibaba.csp.sentinel.annotation.aspectj.SentinelResourceAspect;

/**
 * sentinel 配置类
 * @author Rocket
 */
@Configuration
public class SentinelConfiguration {

    /**
     * 配置sentinel aop支持
     * 例如 热点数据 @SentinelResource
     * @return
     */
    @Bean
    public SentinelResourceAspect sentinelResourceAspect() {
        return new SentinelResourceAspect();
    }
}
