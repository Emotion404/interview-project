package java8.exercise.demo.function;

import java.util.function.Supplier;

public class SupplierDemo {

    public static void main(String[] args) {

        String username = "guancanghai";
        String sex = "male";
        String userInfo = toString(String::new);
        System.out.println(userInfo);
    }

    /**
     * <p>
     *     java.util.function.Supplier<T> 接口仅包含一个无参的方法: T get() 。
     * </p>
     * <p>
     *     用来获取一个泛型参数指定类型的对象数据。由于这是一个函数式接口，
     * 这也就意味着对应的Lambda表达式需要“对外提供”一个符合泛型类型的对象数据。
     * </p> 
     * <p>
     *     意思就是 这是个容器，用来存储需要的数据，取的时候直接get。但是可以使用lambda
     * </p>
     * @param stringAppend the appendedString
     * @return String
     */
    private static String toString(Supplier<String> stringAppend) {
        return stringAppend.get();
    }
}
