package java8.exercise.demo.function;

import java.util.function.Predicate;

/**
 * 有时候我们需要对某种类型的数据进行判断，从而得到一个boolean值结果。这时可以使用java.util.function.Predicate<T> 接口
 */
public class PredicateDemo {

    public static void main(String[] args) {
        String strTest = "12334567";
        if (test(strTest, str -> str.length() > 5)) {
            System.out.println(strTest + "是个长字符串");
        } else {
            System.out.println(strTest + "是个短字符串");
        }
    }

    /**
     * Predicate 接口中包含一个抽象方法: boolean test(T t) 。用于条件判断的场景：
     * @param str 
     * @param predicate
     * @return
     */
    private static Boolean test(String str, Predicate<String> predicate) {

        return predicate.test(str);
    }
}
