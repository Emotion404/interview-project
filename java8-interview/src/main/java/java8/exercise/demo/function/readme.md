#### 函数式接口的特征
 1. 三种方法
 - 唯一的抽象方法
 - 使用default定义普通方法（默认方法），通过对象调用。
 - 使用static定义静态方法，通过接口名调用。
 
 2. 一个新注解@FunctionInterface
 - 注解@FunctionalInterface告诉编译器这是一个函数式接口，明确这个函数中只有一个抽象方法，当你尝试在接口中编写多个抽象方法的时候编译器将不允许，但是可以有多个非抽象方法。
   不过Object类的方法可以定义为抽象方法，因为接口的实现类一定是Object的子类
 - 如果接口被标注了@FunctionalInterface，这个类就必须符合函数式接口的规范
 - 即使一个接口没有标注@FunctionalInterface，如果这个接口满足函数式接口规则，依旧被当作函数式接口。
 
 ##### JDK 1.8 新增加的函数接口包
|   类型  | 接口  | 参数    |  返回值   |   描述   |
| --- | --- | --- | --- | --- |
|供给型接口| Supplier<T> |   None  |  T    |   没有输入，返回一个对象T   |
|功能型函数式接口	|  Function<T, R>   |  T |   R  | 对象转换，T->R |
|消费型接口| Consumer<T> | T  |  void   | 改变对象T内部属性的值 |
|断言型接口| Predicate<T> | T | boolean| 进行逻辑判断，返回boolean值 |