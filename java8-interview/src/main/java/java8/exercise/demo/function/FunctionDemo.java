package java8.exercise.demo.function;

import java.util.function.Function;

/**
 * <p>
 *     java.util.function.Function<T,R> 接口用来根据一个类型的数据得到另一个类型的数据，前者称为前置条件，后者称为后置条件
 * </p>
 */
public class FunctionDemo {

    public static void main(String[] args) {
        Integer result = applyToInteger("10", str -> Integer.parseInt(str));

        Integer andThenResult = andThenMd("10", str -> Integer.parseInt(str), firstRes -> firstRes *= 5);

        System.out.println(andThenResult);
    }

    /**
     * <p>
     *     Function 接口中最主要的抽象方法为: R apply(T t) ，根据类型T的参数获取类型R的结果。
     * </p>
     * <p> 使用的场景例如:将 String 类型转换为 Integer 类型。</p>
     * @return
     */
    private static Integer applyToInteger(String transformStr, Function<String, Integer> transformInteger) {
        return transformInteger.apply(transformStr);
    }

    /**
     * @see Function
     * 
     * <p>
     *     该方法用于“先做什么，再做什么”的场景
     * </p>
     */
    private static Integer andThenMd(String transformStr, Function<String, Integer> firstStep, Function<Integer, Integer> secondStep) {
        return firstStep.andThen(secondStep).apply(transformStr);
    }
}
