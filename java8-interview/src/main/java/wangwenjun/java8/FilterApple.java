package wangwenjun.java8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @auth eric-好好学习，天天向上!
 * @date 2022/5/10
 * @description
 */
public class FilterApple {

    /**
     * 满足lambda的语法的函数是接口只有一个方法
     */
    @FunctionalInterface
    public interface AppleFilter {
        boolean filter(Apple apple);
    }

    public static List<Apple> findApple(List<Apple> apples, AppleFilter appleFilter) {
        List<Apple> list = new ArrayList<>();
        for (Apple apple : apples) {
            if (appleFilter.filter(apple)) {
                list.add(apple);
            }
        }

        return list;
    }

    public static List<Apple> findGreenApple(List<Apple> apples) {
        List<Apple> list = new ArrayList<>();
        for (Apple apple : apples) {
            if ("green".equals(apple.getColor())) {
                list.add(apple);
            }
        }

        return list;
    }

    public static List<Apple> findApple(List<Apple> apples, String color) {
        List<Apple> list = new ArrayList<>();
        for (Apple apple : apples) {
            if (color.equals(apple.getColor())) {
                list.add(apple);
            }
        }

        return list;
    }

    public static void main(String[] args) throws InterruptedException {
        List<Apple> list = Arrays.asList(new Apple("green", 120), new Apple("yellow", 150));
//        List<Apple> greenApples = findGreenApple(list);
//        assert greenApples.size() == 1 : "没有筛选出1个苹果";
//        System.out.println(greenApples);

        List<Apple> apples = findApple(list, apple -> "green".equals(apple.getColor()));
        System.out.println(apples);

        new Thread(() -> {
            System.out.println(Thread.currentThread().getName());
        }).start();

        Thread.currentThread().join();
    }
}
