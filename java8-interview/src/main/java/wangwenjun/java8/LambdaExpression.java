package wangwenjun.java8;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * @auth eric-好好学习，天天向上!
 * @date 2022/5/10
 * @description lambda 表达式
 * 语法：
 * (parameters)->expression
 * (parameters)-> {statements;}
 * ()-> {}
 */
public class LambdaExpression {

    public static void main(String[] args) {
        // 排序
        Comparator<Apple> byColor = new Comparator<Apple>() {
            @Override
            public int compare(Apple o1, Apple o2) {
                return o1.getColor().compareTo(o2.getColor());
            }
        };

        List<Apple> list = Collections.emptyList();
        list.sort(byColor);

        Comparator<Apple> byColor02 = (a1, a2) -> a1.getColor().compareTo(a2.getColor());

        //接收一个参数， 返回一个值
        Function<String, Integer> function = s -> s.length();

        Supplier<Apple> appleSupplier = Apple::new;
    }
}
