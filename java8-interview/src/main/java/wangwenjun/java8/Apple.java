package wangwenjun.java8;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @auth eric-好好学习，天天向上!
 * @date 2022/5/10
 * @description
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Apple {

    private String color;

    private Integer weight;
}
