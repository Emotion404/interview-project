package dynamic.datasource.rw.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.baomidou.dynamic.datasource.annotation.DS;

import dynamic.datasource.rw.constant.DBConstants;
import dynamic.datasource.rw.dataobject.OrderDO;

@Repository
public interface OrderMapper {

    @DS(DBConstants.DATASOURCE_SLAVE)
    OrderDO rwSelectById(@Param("id") Integer id);

    @DS(DBConstants.DATASOURCE_MASTER)
    int insertOrder(OrderDO entity);

}