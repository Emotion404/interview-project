package dynamic.datasource.rw.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.baomidou.dynamic.datasource.annotation.DS;

import dynamic.datasource.rw.constant.DBConstants;
import dynamic.datasource.rw.dataobject.OrderDO;

@Repository
@DS(DBConstants.DATASOURCE_ORDERS)
public interface OrderMapper {

    OrderDO selectById(@Param("id") Integer id);

}