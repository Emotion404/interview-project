package bear.fire.test.innerclass;

/**
 * @auth eric-好好学习，天天向上!
 * @date 2022/3/30
 * @description 匿名内部类
 */
public class Outer4 {
    private void test(final int i) {
        new Service() {

            @Override
            public void method() {
                for (int j = 0; j < i; j++) {
                    System.out.println("匿名内部类");
                }
            }

            @Override
            public void method2(String value) {

            }
        }.method();
    }
}//匿名内部类必须继承或实现一个已有的接口

interface Service {
    void method();

    void method2(String value);
}

/**
 * 为什么
 */
