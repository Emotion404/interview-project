package bear.fire.test.innerclass;

/**
 * @auth eric-好好学习，天天向上!
 * @date 2022/3/30
 * @description 成员内部类
 */
public class Outer2 {

    private int anInt = 1;

    private class Inner {
        public void visit() {
            System.out.println(anInt);
        }
    }

    public static void main(String[] args) {
        Outer2 outer = new Outer2();
        Outer2.Inner inner = outer.new Inner();
        inner.visit();
    }
}
