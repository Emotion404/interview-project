package bear.fire.test.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller("/test")
public class TestCotroller {

    @RequestMapping()
    public String testRedirect() {
        Map<String, String> testMap = new HashMap<>();

        testMap.put("testData", "I am come from testController");

        return "redirect";
    }
}
