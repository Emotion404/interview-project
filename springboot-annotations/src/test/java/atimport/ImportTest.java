package atimport;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;
import zhenglian.annos.AnnosApplication;
import zhenglian.annos.atimport.ConfigA;
import zhenglian.annos.atimport.ServiceInterface;

@SpringBootTest(classes = AnnosApplication.class)
@RunWith(SpringRunner.class)
public class ImportTest {

    @Test
    public void test() {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(ConfigA.class);
        ServiceInterface serviceInterface = applicationContext.getBean(ServiceInterface.class);
        serviceInterface.test();
    }
}
