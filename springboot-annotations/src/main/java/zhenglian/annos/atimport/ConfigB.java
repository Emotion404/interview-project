package zhenglian.annos.atimport;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;

//@Configuration
class ConfigB {

    @Bean
    @ConditionalOnMissingBean
    public ServiceInterface getServiceB() {
        System.out.println("service B init.");
        return new ServiceB();
    }
}

