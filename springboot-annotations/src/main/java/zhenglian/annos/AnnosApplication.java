package zhenglian.annos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnnosApplication {

    public static void main(String[] args) {
        SpringApplication.run(AnnosApplication.class, args);


    }
}
