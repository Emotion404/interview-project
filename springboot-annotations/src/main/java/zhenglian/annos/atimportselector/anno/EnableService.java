package zhenglian.annos.atimportselector.anno;

import org.springframework.context.annotation.Import;
import zhenglian.annos.atimportselector.ServiceImportSelector;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Documented
@Target(ElementType.TYPE)
@Import(ServiceImportSelector.class)
public @interface EnableService {
    String name();
}