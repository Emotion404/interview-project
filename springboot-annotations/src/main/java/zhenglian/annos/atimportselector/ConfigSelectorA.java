package zhenglian.annos.atimportselector;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Import(ServiceImportSelector.class)
@Configuration
public class ConfigSelectorA {

    @Bean
    @ConditionalOnMissingBean
    public ServiceInterface serviceA() {
        return new ServiceA();
    }
}
