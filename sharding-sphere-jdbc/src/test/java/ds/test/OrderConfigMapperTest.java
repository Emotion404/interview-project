package ds.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import sharding.sphere.datasource.ShardingSphereApplication;
import sharding.sphere.datasource.dataobject.OrderConfigDO;
import sharding.sphere.datasource.mapper.OrderConfigMapper;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ShardingSphereApplication.class)
public class OrderConfigMapperTest {

    @Autowired
    private OrderConfigMapper orderConfigMapper;

    @Test
    public void testSelectById() {
        OrderConfigDO orderConfig = orderConfigMapper.selectById(1);
        System.out.println(orderConfig);
    }

}