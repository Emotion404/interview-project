package sharding.sphere.datasource;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = "sharding.sphere.datasource.mapper")
public class ShardingSphereApplication {
}
