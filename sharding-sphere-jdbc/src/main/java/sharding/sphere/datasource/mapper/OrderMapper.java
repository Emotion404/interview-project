package sharding.sphere.datasource.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import sharding.sphere.datasource.dataobject.OrderDO;

import java.util.List;

@Repository
public interface OrderMapper {
    OrderDO selectById(@Param("id") Integer id);

    List<OrderDO> selectListByUserId(@Param("userId") Integer userId);

    int insert(OrderDO orderDO);
}
