package sharding.sphere.datasource.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import sharding.sphere.datasource.dataobject.OrderConfigDO;

@Repository
public interface OrderConfigMapper {
    OrderConfigDO selectById(@Param("id") Integer id);

}
