package sharding.sphere.datasource.dataobject;

import lombok.Data;

@Data
public class OrderConfigDO {

    private Integer id;

    private Integer payTimeout;
}
