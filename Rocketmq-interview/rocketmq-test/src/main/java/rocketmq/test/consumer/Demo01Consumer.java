package rocketmq.test.consumer;

import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import rocketmq.test.message.Demo01Message;

/**
 *  RocketMQMessageListener
 *  声明消费的主题 和 分组
 *  一般情况下 一个主题对应一个分组
 *  好处是： 每个消费者分组职责单一，只消费一个 Topic 。
 *          每个消费者分组是独占一个线程池，这样能够保证多个 Topic 隔离在不同线程池，保证隔离性，从而避免一个 Topic 消费很慢，影响到另外的 Topic 的消费。
 */
@Slf4j
@Component
@RocketMQMessageListener(topic = Demo01Message.TOPIC, consumerGroup = "demo01-consumer-group-" + Demo01Message.TOPIC)
public class Demo01Consumer implements RocketMQListener<Demo01Message> {

    @Override
    public void onMessage(Demo01Message demo01Message) {
        log.info("[onMessage][线程编号:{} 消息内容：{}]", Thread.currentThread().getId(), demo01Message);
    }

}
