package rocketmq.test.message;

import lombok.Data;

@Data
public class Demo01Message {

    /**
     * topic
     */
    public static final String TOPIC = "DEMO_01";

    /**
     * 编号
     */
    private Integer            id;
}
