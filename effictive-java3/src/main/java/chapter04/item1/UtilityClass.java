package chapter04.item1;

/**
 * 因此可以通过包含一个私有构造
 * 器来实现类的非实例化
 */
public class UtilityClass {

    /**
     * Suppress default constructor for noninstantiability
     * 为不可实例化增加默认构造函数
     * <p>
     * AssertionError 异常不是严格要求
     * 的，但是它可以避免不小心在类的内部调用构造器。它保证类在任何情况下都不会被实例化。这个习惯
     * 用法有点违反直觉，好像构造器就是设计成不能调用的一样。因此，如前面所示，添加注释是种明智的
     * 做法。
     */
    private UtilityClass() {
        throw new AssertionError();
    }
}
