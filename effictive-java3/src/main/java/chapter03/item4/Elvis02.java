package chapter03.item4;

import java.io.Serializable;

/**
 * 有两种常见的方法来实现单例。两者都基于保持构造方法私有和导出公共静态成员以提供对唯一实例的访问
 * 在第二个实现单例的方法中，公共成员是一个静态的工厂方法
 */
public class Elvis02 implements Serializable {

    private transient static final Elvis02 INSTANCE = new Elvis02();

    private Elvis02() {
    }

    /**
     * 所有对 Elvis.getInstance 的调用都返回相同的对象引用，并且不会创建其他的 Elvis 实例
     *
     * @return
     */
    public static Elvis02 getInstance() {
        return INSTANCE;
    }


    /**
     * readResolve method to preserve singleton property
     * 的单例类变成是可序列化的 （第 12 章），仅仅将 implements Serializable 添加到声明中是不够的。为了保证单例模式不被破坏，必须声明所有的实例字段为
     * transient ，并提供一个 readResolve 方法（
     *
     * @return
     */
    private Object readResolve() {
        // Return the one true Elvis and let the garbage collector
        // take care of the Elvis impersonator.
        return INSTANCE;
    }

    public static void main(String[] args) {
        Elvis02 elvis = Elvis02.getInstance();
    }
}
