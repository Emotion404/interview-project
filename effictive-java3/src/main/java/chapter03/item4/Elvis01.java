package chapter03.item4;

/**
 * 有两种常见的方法来实现单例。两者都基于保持构造方法私有和导出公共静态成员以提供对唯一实例的访问
 */
public class Elvis01 {

    /**
     * 公共静态成员 对外暴露
     */
    public static final Elvis01 INSTANCE = new Elvis01();

    /**
     * 保持构造方法私有
     * <p>
     * 缺少一个公共的或受保护的构造方法， 保持他的全局唯一性
     * <p>
     * 一旦 Elvis 类被初始化，一个 Elvis 的实例就会存在——不多也不少
     * <p>
     * ：特权客户端可以使用
     * AccessibleObject.setAccessible 方法，以反射方式调用私有构造方法 。如果需
     * 要防御此攻击，请修改构造函数，使其在请求创建第二个实例时抛出异常
     */
    private Elvis01() {
    }

    public void leaveTheBuilding() {
    }
}
