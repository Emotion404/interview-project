package chapter02.item2;

/**
 * Java Bean 模式
 * <p>
 * 为每个属性设置get set 方法
 * <p>
 * 营养配料表
 */
public class NutritionFacts {

    /**
     * 分量大小
     */
    private int servingSize;

    /**
     * 分量
     */
    private int servings;

    /**
     * 卡路里
     */
    private int calories;

    /**
     * 脂肪
     */
    private int fat;

    /**
     * 钠
     */
    private int sodium;

    public void setServingSize(int servingSize) {
        this.servingSize = servingSize;
    }

    public void setServings(int servings) {
        this.servings = servings;
    }

    public int getServingSize() {
        return getServingSize();
    }


    // ......

    public static void main(String[] args) {
        // JavaBeans 模式本身有严重的缺陷。由于构造方法被分割成了多次调用，所以在构造过
        //程中 JavaBean 可能处于不一致的状态。该类没有通过检查构造参数参数的有效性来强制一致性的选
        //项。在不一致的状态下尝试使用对象可能会导致一些错误，这些错误与平常代码的BUG很是不同，因此
        //很难调试。一个相关的缺点是，JavaBeans 模式排除了让类不可变的可能性，并且需要
        //程序员增加工作以确保线程安全。
        NutritionFacts cola = new NutritionFacts();
        cola.setServings(1);

    }
}
