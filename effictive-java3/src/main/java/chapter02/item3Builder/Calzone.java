package chapter02.item3Builder;

/**
 *
 */
public class Calzone extends Pizza {
    private final boolean sauceInside;

    /**
     * NyPizza.Builder 的 build 方法返回 NyPizza ，而 Calzone.Builder 中的 build 方法返回 Calzone 。 这种技
     * 术，其一个子类的方法被声明为返回在超类中声明的返回类型的子类型，称为协变返回类型
     */
    public static class Builder extends Pizza.Builder<Builder> {
        private boolean sauceInside = false;

        public Builder sauceInside() {
            sauceInside = true;
            return this;
        }

        @Override
        public Calzone build() {
            return new Calzone(this);
        }

        @Override
        protected Builder self() {
            return this;
        }
    }

    private Calzone(Builder builder) {
        super(builder);
        sauceInside = builder.sauceInside;
    }
}