package chapter02.item1;

/**
 * 伸缩性构造模式- 伸缩性不好！！
 * <p>
 * 传统上，程序员使用了可伸缩（telescoping constructor）构造方法模式，在这种模式中，首先提供一个只有必需参数的构造方法，接着提供增加了
 * 一个可选参数的构造函数，然后提供增加了两个可选参数的构造函数，等等，最终在构造函数中包含所
 * 有必需和可选参数
 * <p>
 * 营养配料表
 */
public class NutritionFacts {

    /**
     * 分量大小
     */
    private final int servingSize;

    /**
     * 分量
     */
    private final int servings;

    /**
     * 卡路里
     */
    private final int calories;

    /**
     * 脂肪
     */
    private final int fat;

    /**
     * 钠
     */
    private final int sodium; // (mg/serving) optional

    /**
     * 糖类
     */
    private final int carbohydrate;

    public NutritionFacts(int servingSize, int servings) {
        this(servingSize, servings, 0);
    }

    public NutritionFacts(int servingSize, int servings, int calories) {
        this(servingSize, servings, calories, 0);
    }

    public NutritionFacts(int servingSize, int servings, int calories, int fat) {
        this(servingSize, servings, calories, fat, 0);
    }

    public NutritionFacts(int servingSize, int servings, int calories, int fat, int sodium) {
        this(servingSize, servings, calories, fat, sodium, 0);
    }

    public NutritionFacts(int servingSize, int servings, int calories, int fat, int sodium, int carbohydrate) {
        this.servingSize = servingSize;
        this.servings = servings;
        this.calories = calories;
        this.fat = fat;
        this.sodium = sodium;
        this.carbohydrate = carbohydrate;
    }

    public static void main(String[] args) {
        //通常情况下，这个构造方法的调用需要许多你不想设置的参数，但是你不得不为它们传递一个值。
        //在这种情况下，我们为 fat 属性传递了 0 值。「只有」六个参数可能看起来还好，但随着参数数量的增加，它很快就会失控。
        NutritionFacts cola = new NutritionFacts(240, 8, 100, 0, 35, 27);

    }
}
