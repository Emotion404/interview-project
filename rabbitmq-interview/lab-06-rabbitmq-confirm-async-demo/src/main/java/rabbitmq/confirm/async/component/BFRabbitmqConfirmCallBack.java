package rabbitmq.confirm.async.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

/**
 * 当producer 发送消息给broker后，如果成功，broker返回confirm标识ack 使用Rabbitmq.ConfirmCallback
 */
@Component
public class BFRabbitmqConfirmCallBack implements RabbitTemplate.ConfirmCallback {
    private Logger logger = LoggerFactory.getLogger(getClass());

    public BFRabbitmqConfirmCallBack(RabbitTemplate rabbitTemplate) {
        rabbitTemplate.setConfirmCallback(this);
    }

    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        if (ack) {
            logger.info("[confirm][Confirm 成功 correlationData: {}]", correlationData);
        } else {
            logger.error("[confirm][Confirm 失败 correlationData: {} cause: {}]", correlationData, cause);
        }
    }
}
