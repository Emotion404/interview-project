package rabbitmq.confirm.async.message;

import java.io.Serializable;

import lombok.Data;

@Data
public class Message implements Serializable {

    public static class Confirm {
        public static final String CONFIRM_EXCHANGE = "confirm_async_exchange";
        public static final String CONFIRM_QUEUE    = "confirm_async_queue";
        public static final String ROUTING_KEY      = "confirm_async_routing_key";
    }

    // 传输的信息
    private Integer id;
}
