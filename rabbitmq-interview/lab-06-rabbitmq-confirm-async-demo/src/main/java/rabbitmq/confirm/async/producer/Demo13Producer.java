package rabbitmq.confirm.async.producer;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import rabbitmq.confirm.async.message.Message;

@Component
public class Demo13Producer {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void syncSend(Integer id) {
        // 创建 Demo13Message 消息
        Message message = new Message();
        message.setId(id);
        // 同步发送消息
        rabbitTemplate.convertAndSend(Message.Confirm.CONFIRM_EXCHANGE, Message.Confirm.ROUTING_KEY, message);
    }

    public void syncSendReturn(Integer id) {
        // 创建 Demo13Message 消息
        Message message = new Message();
        message.setId(id);
        // 同步发送消息
        rabbitTemplate.convertAndSend(Message.Confirm.CONFIRM_EXCHANGE, "error", message);
    }

}
