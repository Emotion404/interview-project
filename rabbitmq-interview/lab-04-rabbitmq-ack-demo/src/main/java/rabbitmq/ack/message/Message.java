package rabbitmq.ack.message;

import java.io.Serializable;

import lombok.Data;

@Data
public class Message implements Serializable {

    public static class Ack {
        public static final String ACK_EXCHANGE = "ack_exchange";
        public static final String ACK_QUEUE    = "ack_queue";
        public static final String ROUTING_KEY  = "ack_routing_key";
    }

    // 传输的信息
    private Integer id;
}
