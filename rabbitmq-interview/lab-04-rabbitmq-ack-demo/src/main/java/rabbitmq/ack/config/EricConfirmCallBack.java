package rabbitmq.ack.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author eric
 * 发送的可靠性 confirm 模式
 */
@Slf4j
@Component
public class EricConfirmCallBack implements RabbitTemplate.ConfirmCallback {

    @Resource
    RabbitTemplate rabbitTemplate;

 
    /**
     * @param correlationData
     * @param ack
     * @param cause
     */
    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        log.info("correlationData: {}, \nack:{} , \n cause: {}", correlationData, ack, cause);
    }
}
