package rabbitmq.ack.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import rabbitmq.ack.message.Message;

/**
 *
 */
@Component
public class RabbitmqConfig {

    public static class AckExchangeConfiguration {

        @Bean
        public Queue ackQueue() {
            return new Queue(Message.Ack.ACK_QUEUE, true, // durable: 是否持久化
                false, // exclusive: 是否排它
                false); // autoDelete: 是否自动删除
        }

        @Bean
        public DirectExchange ackExchange() {
            return new DirectExchange(Message.Ack.ACK_EXCHANGE, true, // durable: 是否持久化
                false); // exclusive: 是否排它
        }

        @Bean
        public Binding dirBinding() {
            return BindingBuilder.bind(ackQueue()).to(ackExchange()).with(Message.Ack.ROUTING_KEY);
        }
    }

}
