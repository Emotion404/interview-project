package rabbitmq.ack.producer;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import rabbitmq.ack.message.Message;

@Configuration
public class AckProducer {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void syncSend(Integer id) {
        // 创建 Demo12Message 消息
        Message message = new Message();
        message.setId(id);
        // 同步发送消息
        rabbitTemplate.convertAndSend(Message.Ack.ACK_EXCHANGE, Message.Ack.ROUTING_KEY, message);
    }
}
