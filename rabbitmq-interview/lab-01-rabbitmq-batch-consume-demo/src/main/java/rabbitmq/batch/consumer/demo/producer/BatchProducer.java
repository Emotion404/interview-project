package rabbitmq.batch.consumer.demo.producer;

import org.springframework.amqp.rabbit.core.BatchingRabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import rabbitmq.batch.consumer.demo.message.Message;

@Component
public class BatchProducer {

    @Autowired
    private BatchingRabbitTemplate batchingRabbitTemplate;

    public void syncSend(Integer id) {
        // 创建 Demo05Message 消息
        Message message = new Message();
        message.setId(id);
        // 同步发送消息
        batchingRabbitTemplate.convertAndSend(Message.EXCHANGE, Message.ROUTING_KEY, message);
    }

}
