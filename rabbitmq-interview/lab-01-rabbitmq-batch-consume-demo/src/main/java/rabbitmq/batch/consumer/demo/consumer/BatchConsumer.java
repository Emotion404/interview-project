package rabbitmq.batch.consumer.demo.consumer;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import rabbitmq.batch.consumer.demo.message.Message;

@Component
@RabbitListener(queues = Message.QUEUE, containerFactory = "consumerBatchContainerFactory")
public class BatchConsumer {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @RabbitHandler
    public void onMessage(List<Message> messages) {
        logger.info("[onMessage][线程编号:{} 消息数量：{}]", Thread.currentThread().getId(), messages.size());
    }

    //    @RabbitHandler(isDefault = false)
    //    public void onMessageX(List<Message> messages) {
    //        logger.info("[onMessage][线程编号:{} 消息数量：{}]", Thread.currentThread().getId(), messages.size());
    //    }

}