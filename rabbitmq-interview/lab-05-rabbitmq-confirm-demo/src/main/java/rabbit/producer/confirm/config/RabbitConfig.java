package rabbit.producer.confirm.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import rabbit.producer.confirm.message.Message;

@Configuration
public class RabbitConfig {

    /**
     * Direct Exchange 示例的配置类
     */
    public static class ConfirmExchangeDemoConfiguration {

        // 创建 Queue
        @Bean
        public Queue demo13Queue() {
            return new Queue(Message.Confirm.CONFIRM_QUEUE, // Queue 名字
                true, // durable: 是否持久化
                false, // exclusive: 是否排它
                false); // autoDelete: 是否自动删除
        }

        // 创建 Direct Exchange
        @Bean
        public DirectExchange demo13Exchange() {
            return new DirectExchange(Message.Confirm.CONFIRM_EXCHANGE, true, // durable: 是否持久化
                false); // exclusive: 是否排它
        }

        // 创建 Binding
        // Exchange：Demo13Message.EXCHANGE
        // Routing key：Demo13Message.ROUTING_KEY
        // Queue：Demo13Message.QUEUE
        @Bean
        public Binding demo13Binding() {
            return BindingBuilder.bind(demo13Queue()).to(demo13Exchange()).with(Message.Confirm.ROUTING_KEY);
        }

    }

}
