package eric.rabbit.sequence.config;

import eric.rabbit.sequence.common.Constant;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author eric
 * mq 初始化 配置
 */
@Configuration
public class MqInitConfig {

    @Bean
    public Queue saveQueue() {
        return new Queue(Constant.SEQUENCE_QUEUE_SAVE, false, false, false);
    }

    @Bean
    public Queue deleteQueue() {
        return new Queue(Constant.SEQUENCE_QUEUE_DELETE, false, false, false);
    }

    @Bean
    public Queue updateQueue() {
        return new Queue(Constant.SEQUENCE_QUEUE_UPDATE, false, false, false);
    }


    @Bean
    public Binding saveBinding() {
        return BindingBuilder.bind(saveQueue()).to(sequenceExchange()).with(Constant.SAVE_ROUTING_KEY);
    }

    @Bean
    public Binding deleteBinding() {
        return BindingBuilder.bind(deleteQueue()).to(sequenceExchange()).with(Constant.DELETE_ROUTING_KEY);
    }

    @Bean
    public Binding updateBinding() {
        return BindingBuilder.bind(updateQueue()).to(sequenceExchange()).with(Constant.UPDATE_ROUTING_KEY);
    }
    
    @Bean
    public DirectExchange sequenceExchange() {
        return new DirectExchange(Constant.SEQUENCE_EXCHANGE, false, false);
    }
}
