package eric.rabbit.sequence.common;

/**
 * @author eric
 */
public class Constant {

    public static final String SEQUENCE_EXCHANGE = "sequence_exchange";
    public static final String SEQUENCE_QUEUE_DELETE = "sequence_queue_delete";
    public static final String SEQUENCE_QUEUE_SAVE = "sequence_queue_save";
    public static final String SEQUENCE_QUEUE_UPDATE = "sequence_queue_update";

    public static final String SAVE_ROUTING_KEY = "save_routing_key";
    public static final String DELETE_ROUTING_KEY = "delete_routing_key";
    public static final String UPDATE_ROUTING_KEY = "update_routing_key";
}
