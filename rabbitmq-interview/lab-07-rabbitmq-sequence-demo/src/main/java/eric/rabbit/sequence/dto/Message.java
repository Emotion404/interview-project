package eric.rabbit.sequence.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class Message implements Serializable {

    private String id;
    
    private Integer order;

    private String body;
}
