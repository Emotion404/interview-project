package eric.rabbit.sequence.consumer;

import eric.rabbit.sequence.common.Constant;
import eric.rabbit.sequence.dto.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * 顺序消费
 */
@Component
@RabbitListener(queues = {Constant.SEQUENCE_QUEUE_DELETE, Constant.SEQUENCE_QUEUE_SAVE, Constant.SEQUENCE_QUEUE_UPDATE})
public class SequenceConsumer {

    private Queue<List<Message>> localQueue = new ConcurrentLinkedQueue();

}
