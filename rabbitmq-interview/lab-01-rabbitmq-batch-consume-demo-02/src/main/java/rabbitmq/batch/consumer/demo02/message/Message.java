package rabbitmq.batch.consumer.demo02.message;

import java.io.Serializable;

import lombok.Data;

@Data
public class Message implements Serializable {

    public static final String QUEUE       = "queue_batch_consumer02";

    public static final String EXCHANGE    = "exchange_batch_consumer02";

    public static final String ROUTING_KEY = "routing_key";

    /**
     * 编号
     */
    private Integer            id;

    @Override
    public String toString() {
        return "Demo05Message{" + "id=" + id + '}';
    }

}
