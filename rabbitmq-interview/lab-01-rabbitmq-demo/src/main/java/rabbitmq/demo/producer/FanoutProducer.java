package rabbitmq.demo.producer;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import rabbitmq.demo.message.Message;

@Component
public class FanoutProducer {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void syncSend(Integer id) {
        // 创建 Demo03Message 消息
        Message message = new Message();
        message.setId(id);
        // 同步发送消息
        rabbitTemplate.convertAndSend(Message.Fanout.FANOUT_EXCHANGE, null, message);
    }
}
