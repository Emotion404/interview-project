package rabbitmq.demo.producer;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import rabbitmq.demo.message.Message;

@Configuration
public class TopicProducer {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void syncSend(Integer id, String routingKey) {
        // 创建 Demo02Message 消息
        Message message = new Message();
        message.setId(id);
        // 同步发送消息
        rabbitTemplate.convertAndSend(Message.Topic.TOPIC_EXCHANGE, routingKey, message);
    }
}
