package rabbitmq.demo.producer;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.util.concurrent.ListenableFuture;

import rabbitmq.demo.message.Message;

@Configuration
public class DirectProducer {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void syncSend(Integer id) {
        Message message = new Message();
        message.setId(id);
        rabbitTemplate.convertAndSend(Message.Direct.DIR_EXCHANGE, Message.Direct.ROUTING_KEY, message);
    }

    public void syncDefault(Integer id) {
        Message message = new Message();
        message.setId(id);
        rabbitTemplate.convertAndSend(Message.Direct.DIR_QUEUE, message);
    }

    @Async
    public ListenableFuture<Void> asyncSend(Integer id) {
        try {
            // 发送消息
            this.syncSend(id);
            // 返回成功的 Future
            return AsyncResult.forValue(null);
        } catch (Throwable ex) {
            // 返回异常的 Future
            return AsyncResult.forExecutionException(ex);
        }
    }
}
