package rabbitmq.demo.controller;

import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rabbitmq.demo.producer.DirectProducer;

import java.util.concurrent.CountDownLatch;

@RestController
@RequestMapping("/rabbit")
public class RabbitMqTestController {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private DirectProducer producer;

    @SneakyThrows
    @GetMapping("/direct")
    public void directTest() {
        int id = (int) (System.currentTimeMillis() / 1000);
        producer.syncSend(id);
        logger.info("[testSyncSend][发送编号：[{}] 发送成功]", id);

        // 阻塞等待，保证消费
        new CountDownLatch(1).await();
    }
}
