package rabbitmq.demo.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import rabbitmq.demo.message.Message;

/**
 *
 */
@Component
public class RabbitmqConfig {

    public static class DirectExchangeConfiguration {

        @Bean
        public Queue dirQueue() {
            return new Queue(Message.Direct.DIR_QUEUE, true, // durable: 是否持久化
                false, // exclusive: 是否排它
                false); // autoDelete: 是否自动删除
        }

        @Bean
        public DirectExchange directExchange() {
            return new DirectExchange(Message.Direct.DIR_EXCHANGE, true, // durable: 是否持久化
                false); // exclusive: 是否排它
        }

        @Bean
        public Binding dirBinding() {
            return BindingBuilder.bind(dirQueue()).to(directExchange()).with(Message.Direct.ROUTING_KEY);
        }

    }

    public static class TopicExchangeConfiguration {

        @Bean
        public Queue topicQueue() {
            return new Queue(Message.Topic.TOPIC_QUEUE, true, // durable: 是否持久化
                false, // exclusive: 是否排它
                false); // autoDelete: 是否自动删除
        }

        @Bean
        public TopicExchange topicExchange() {
            return new TopicExchange(Message.Topic.TOPIC_EXCHANGE, true, // durable: 是否持久化
                false); // exclusive: 是否排它
        }

        @Bean
        public Binding topicBinding() {
            return BindingBuilder.bind(topicQueue()).to(topicExchange()).with(Message.Topic.ROUTING_KEY);
        }
    }

    public static class FanoutExchangeConfiguration {
        @Bean
        public Queue queueA() {
            return new Queue(Message.Fanout.FANOUT_QUEUE_A, true, true, false);
        }

        @Bean
        public Queue queueB() {
            return new Queue(Message.Fanout.FANOUT_QUEUE_B, true, true, false);
        }

        @Bean
        public FanoutExchange fanoutExchange() {
            return new FanoutExchange(Message.Fanout.FANOUT_EXCHANGE, true, true);
        }

        @Bean
        public Binding exchangeBindA() {
            return BindingBuilder.bind(queueA()).to(fanoutExchange());
        }

        @Bean
        public Binding exchangeBindB() {
            return BindingBuilder.bind(queueB()).to(fanoutExchange());
        }

    }
}
