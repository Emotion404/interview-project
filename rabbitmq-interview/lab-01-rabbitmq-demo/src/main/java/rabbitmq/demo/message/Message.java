package rabbitmq.demo.message;

import java.io.Serializable;

import lombok.Data;

@Data
public class Message implements Serializable {

    public static class Direct {
        public static final String DIR_EXCHANGE = "dir_exchange";
        public static final String DIR_QUEUE    = "dir_queue";
        public static final String ROUTING_KEY  = "direct";

    }

    public static class Topic {
        public static final String TOPIC_EXCHANGE = "topic_exchange";
        public static final String TOPIC_QUEUE    = "topic_queue";
        public static final String ROUTING_KEY    = "#.yu.nai";
    }

    public static class Fanout {
        public static final String FANOUT_QUEUE_A  = "fanout_queue_a";
        public static final String FANOUT_QUEUE_B  = "fanout_queue_b";
        public static final String FANOUT_EXCHANGE = "fanout_exchange";
    }

    // 传输的信息
    private Integer id;
}
