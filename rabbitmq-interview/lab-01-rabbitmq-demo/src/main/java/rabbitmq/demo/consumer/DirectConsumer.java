package rabbitmq.demo.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import rabbitmq.demo.message.Message;

@Slf4j
@Component
@RabbitListener(queues = Message.Topic.TOPIC_QUEUE)
public class DirectConsumer {

    @RabbitHandler
    public void onMessage(Message message) {
        log.info("message -->{}", message.getId());
    }

}
