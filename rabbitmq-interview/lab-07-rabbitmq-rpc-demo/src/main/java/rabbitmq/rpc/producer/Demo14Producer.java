package rabbitmq.rpc.producer;// Demo14Producer.java

import java.util.UUID;

import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import rabbitmq.rpc.message.Message;

@Component
public class Demo14Producer {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public String syncSend(Integer id) {
        // 创建 Demo01Message 消息
        Message message = new Message();
        message.setId(id);
        // <1> 创建 CorrelationData 对象
        CorrelationData correlationData = new CorrelationData(UUID.randomUUID().toString());
        // <2> 同步发送消息，并接收结果
        return (String) rabbitTemplate.convertSendAndReceive(Message.Rpc.RPC_EXCHANGE, Message.Rpc.ROUTING_KEY, message, correlationData);
    }

}