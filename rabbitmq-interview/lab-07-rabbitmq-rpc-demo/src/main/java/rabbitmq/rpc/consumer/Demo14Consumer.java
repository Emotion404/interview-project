package rabbitmq.rpc.consumer;// Demo14Consumer.java

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import rabbitmq.rpc.message.Message;

@Component
@RabbitListener(queues = Message.Rpc.RPC_QUEUE)
public class Demo14Consumer {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @RabbitHandler
    public String onMessage(Message message) {
        logger.info("[onMessage][线程编号:{} 消息内容：{}]", Thread.currentThread().getId(), message);
        // 返回结果
        return "nicai";
    }

}