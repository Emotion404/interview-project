package rabbitmq.rpc.message;

import java.io.Serializable;

import lombok.Data;

@Data
public class Message implements Serializable {

    public static class Rpc {
        public static final String RPC_EXCHANGE = "rpc_exchange";
        public static final String RPC_QUEUE    = "rpc_queue";
        public static final String ROUTING_KEY  = "rpc_routing_key";
    }

    // 传输的信息
    private Integer id;
}
