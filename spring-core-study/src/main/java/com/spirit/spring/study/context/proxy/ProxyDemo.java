package com.spirit.spring.study.context.proxy;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @auth eric-好好学习，天天向上!
 * @date 2022/4/5
 * @description
 */
public class ProxyDemo {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("/springContext.xml");
        // 根据bean获取的默认的就是代理对象
        context.getBean("userService");
    }
}
