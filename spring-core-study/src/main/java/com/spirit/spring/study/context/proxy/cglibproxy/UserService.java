package com.spirit.spring.study.context.proxy.cglibproxy;

import com.spirit.spring.study.context.entity.Student;

/**
 * @auth eric-好好学习，天天向上!
 * @date 2022/4/5
 * @description
 */
public class UserService {

    public void login(String username, String password) {
        System.out.println("user login");
    }

    public void register(Student student) {
        System.out.println("userService register student");
    }
}
