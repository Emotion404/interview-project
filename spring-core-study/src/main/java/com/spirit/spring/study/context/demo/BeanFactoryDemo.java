package com.spirit.spring.study.context.demo;

import com.spirit.spring.study.context.entity.Student;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;

/**
 * @author eric - 好好学习，天天向上！
 * @date 2022/2/25
 * @description
 */
@SuppressWarnings("all")
public class BeanFactoryDemo {

    public static void main(String[] args) {
        XmlBeanFactory xmlBeanFactory =
                new XmlBeanFactory(new ClassPathResource("springContext.xml"));

        Student student = (Student) xmlBeanFactory.getBean("student");
        System.out.println(student.getName());
    }
}
