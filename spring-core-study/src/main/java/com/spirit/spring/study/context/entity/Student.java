package com.spirit.spring.study.context.entity;

import org.springframework.beans.factory.BeanNameAware;

/**
 * @author eric - 好好学习，天天向上！
 * @date 2022/2/25
 * @description
 */
public class Student implements BeanNameAware {

    private String name = "spirit";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBeanName(String name) {
        System.out.println("bean name :" + name);
    }
}
