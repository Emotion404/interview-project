package com.spirit.spring.study.context.entity;

import org.springframework.beans.factory.BeanNameAware;

/**
 * @author eric - 好好学习，天天向上！
 * @date 2022/2/25
 * @description
 */
public class BeanNameAwareImpl implements BeanNameAware {

    private String beanName;

    public String getBeanName() {
        return beanName;
    }
    
    public void setBeanName(String s) {
        this.beanName = s;
    }
}
