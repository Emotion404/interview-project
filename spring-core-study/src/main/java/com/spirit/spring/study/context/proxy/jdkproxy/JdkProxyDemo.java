package com.spirit.spring.study.context.proxy.jdkproxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @auth eric-好好学习，天天向上!
 * @date 2022/4/5
 * @description
 */
public class JdkProxyDemo {

    public static void main(String[] args) {
//        UserService userService = new UserServiceImpl;

        InvocationHandler handler = new InvocationHandler() {
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                return null;
            }
        };

//        Proxy.newProxyInstance(JdkProxyDemo.class.getClassLoader(), userService.getClass().getInterfaces(), handler);
    }
}
