package com.spirit.spring.study.context.proxy;

import org.springframework.aop.MethodBeforeAdvice;

import java.lang.reflect.Method;

/**
 * @auth eric-好好学习，天天向上!
 * @date 2022/4/5
 * @description
 */
public class Before implements MethodBeforeAdvice {
 
    public void before(Method method, Object[] objects, Object o) throws Throwable {
        System.out.println("------------------- method before advice log --------------------");
    }
}
