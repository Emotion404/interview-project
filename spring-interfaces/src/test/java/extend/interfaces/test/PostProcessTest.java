package extend.interfaces.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import spring.interfaces.ExtendsApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ExtendsApplication.class)
public class PostProcessTest {

    @Test
    public void postProcessTest() {
    }
}
