package extend.interfaces.test;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import spring.interfaces.ExtendsApplication;
import spring.interfaces.aware.example.User;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ExtendsApplication.class)
public class AwareTest {

    @Resource
    User user;

    @Test
    public void awareTest() {
        System.out.println(String.format("user info- name %s, beanName %s", user.getName(), user.getId()));
        // console info: user info- name zhenglian, beanName zhangsan
    }
}
