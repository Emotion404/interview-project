package extend.interfaces.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;
import spring.interfaces.ExtendsApplication;
import spring.interfaces.others.factorybean.MainConfig1;
import spring.interfaces.others.factorybean.MyFactoryBeanService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ExtendsApplication.class)
public class FactoryBeanTest {

    @Test
    public void test() throws Exception {
        ApplicationContext app = new AnnotationConfigApplicationContext(MainConfig1.class);

        // 一个Bean如果实现了FactoryBean接口，那么根据该Bean的名称获取到的实际上是getObject()返回的对象，而不是这个Bean自身实例，
        //  如果要获取这个Bean自身实例，那么需要在名称前面加上'&'符号
        MyFactoryBeanService myFactoryBean = (MyFactoryBeanService) app.getBean("myFactoryBean");
        myFactoryBean.testPrint();
        ((AnnotationConfigApplicationContext) app).close();
    }
}
