### 2. BeanPostProcessor系列
- BeanPostProcessor
``` text
运行顺序

===Spring IOC容器实例化Bean===
===调用BeanPostProcessor的postProcessBeforeInitialization方法===
===调用bean实例的初始化方法===
===调用BeanPostProcessor的postProcessAfterInitialization方法===
即 在bean实例初始化 `前后` 调用
```
![BeanPostProcessor调用顺序图](https://upload-images.jianshu.io/upload_images/13150128-bb5c9389cd0acc6c.png?imageMogr2/auto-orient/strip|imageView2/2/w/844/format/webp)

- BeanFactoryPostProcessor
```text
bean工厂的bean属性处理容器，
说通俗一些就是可以管理我们的bean工厂内所有的beandefinition（未实例化）数据，
可以随心所欲的修改属性。

注册BeanFactoryPostProcessor的实例，需要重载

void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException;

通过beanFactory可以获取bean的示例或定义等。同时可以修改bean的属性，这是和BeanPostProcessor最大的区别。
```

- InstantiationAwareBeanPostProcessor
```text
方法会在bean的实例化前后被调用(注意实例化和初始化的区别，先实例化，再初始化)
```

- MergedBeanDefinitionPostProcessor
```text
方法会在bean的实例化之后调用，但是在InstantiationAwareBeanPostProcessor的后置After方法之前调用
```

- 调用顺序总结
```text
======BeanFactoryPostProcessor======
======InstantiationAwareBeanPostProcessor#before=============
1. 实例化
======MergedBeanDefinitionPostProcessor======= 
======InstantiationAwareBeanPostProcessor#after==============
======BeanPostProcessor#before========
2. 初始化
======BeanPostProcessor#after=========

```
