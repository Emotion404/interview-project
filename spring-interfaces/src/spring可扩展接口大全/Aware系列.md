## Spring 扩展 接口

### 1.  Aware系列
![Aware接口](https://upload-images.jianshu.io/upload_images/3397380-6ef519bbc705ce28.png?imageMogr2/auto-orient/strip|imageView2/2/w/654/format/webp)
- ApplicaitionContextAware
- ApplicationEventPublisherAware
- BeanClassLoaderAware
- BeanFactoryAware
- BeanNameAware
- EmbeddedValueResolverAware
- EnvironmentAware
- ImportAware
- LoadTimeWeaverAware
- MessageSoueceAware
- NotificationPublisherAware
- ResourceLoaderAware
- ServletConfigware
- ServletContextAware

【aware】: 感知的，意识到的
所以这些接口从字面上意思就是能够感知到`Aware`前面的的信息

例如： `BeanNameAware`, 实现`BeanNameAware`接口, 即可以让该`Bean` 感知到自身的`BeanName`(对应Spring容器的BeanId属性), 感知到`BeanName`后就可以获取`BeanName`;
`ApplicationContextAware` 就可以获取到`ApplicationContext`
`BeanFactoryAware` 就可以获取到`BeanFactory`
