package spring.interfaces.aware.example;

import org.springframework.beans.factory.BeanNameAware;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class User implements BeanNameAware {

    private String id;

    private String name;

    private String address;

    @Override
    public void setBeanName(String beanName) {
        this.id = beanName;
    }
}
