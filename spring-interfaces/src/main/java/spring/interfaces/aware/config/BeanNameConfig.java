package spring.interfaces.aware.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import spring.interfaces.aware.example.User;

@Configuration
public class BeanNameConfig {

    @Bean
    public User zhangsan() {
        User user = new User();
        user.setName("zhenglian").setAddress("suzhou");
        return user;
    }
}
