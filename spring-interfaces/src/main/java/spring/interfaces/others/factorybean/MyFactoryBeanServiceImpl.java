package spring.interfaces.others.factorybean;

class MyFactoryBeanServiceImpl implements MyFactoryBeanService {

    @Override
    public void testPrint() {
        System.out.println("this is my factory bean test.");
    }
}