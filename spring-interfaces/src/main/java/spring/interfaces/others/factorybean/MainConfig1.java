package spring.interfaces.others.factorybean;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("spring.interfaces.others.factorybean")
@PropertySource(value = "classpath:/application.properties")
public class MainConfig1 {

}