package spring.interfaces.others.factorybean;

public interface MyFactoryBeanService {
    void testPrint();
}