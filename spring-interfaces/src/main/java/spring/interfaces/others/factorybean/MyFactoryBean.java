package spring.interfaces.others.factorybean;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.stereotype.Component;

/**
 * 一个Bean如果实现了FactoryBean接口，那么根据该Bean的名称获取到的实际上是getObject()返回的对象，而不是这个Bean自身实例，
 * 如果要获取这个Bean自身实例，那么需要在名称前面加上'&'符号
 */
@Component
public class MyFactoryBean implements FactoryBean<MyFactoryBeanService> {

    @Override
    public MyFactoryBeanService getObject() throws Exception {
        return new MyFactoryBeanServiceImpl();
    }

    @Override
    public Class<?> getObjectType() {
        return MyFactoryBeanService.class;
    }

    /**
     * 通过FactoryBean创建对象，创建对象是否单例是由该方法的返回值决定的
     *
     * @return
     */
    @Override
    public boolean isSingleton() {
        return false;
    }
}



