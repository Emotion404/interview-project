package spring.interview.beanregister;

/**
 * @author eric
 * @date 2021/12/20 21:34
 * @description
 */
public interface IUserDao {

    String queryUserInfo();
}
