package spring.interview.beanregister;

import org.springframework.beans.factory.FactoryBean;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * @author eric
 * @date 2021/12/20 21:35
 * @description
 */
public class ProxyBeanFactory implements FactoryBean {

    @Override
    public Object getObject() throws Exception {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        Class[] classes = {IUserDao.class};
        InvocationHandler invocationHandler = ((proxy, method, args) -> "你被代理了: " + method.getName());

        return Proxy.newProxyInstance(classLoader, classes, invocationHandler);
    }

    @Override
    public Class<?> getObjectType() {
        return IUserDao.class;
    }
}
