package spring.interview.lookup;

/**
 * @author eric
 * @date 2021/12/22 21:33
 * @description
 */
public class Honda implements Car {
    @Override
    public String run() {
        return "Honda run";
    }
}
