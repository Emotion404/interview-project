package spring.interview.lookup;

/**
 * @author eric
 * @date 2021/12/22 21:32
 * @description
 */
public interface Car {

    String run();
}
