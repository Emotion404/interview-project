package spring.interview.lookup;

/**
 * @author eric
 * @date 2021/12/22 21:34
 * @description
 */
public abstract class Run {

    public void commonRun() {
        System.out.println(getCar().run());
    }

    abstract Car getCar();
}
