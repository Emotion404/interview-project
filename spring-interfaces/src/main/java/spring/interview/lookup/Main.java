package spring.interview.lookup;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author eric
 * @date 2021/12/22 21:35
 * @description
 */
public class Main {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring-config.xml");
        Run run = (Run) context.getBean("run");
        run.commonRun();
    }
}
