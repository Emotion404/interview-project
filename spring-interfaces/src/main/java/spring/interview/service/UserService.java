package spring.interview.service;

/**
 * @author eric
 * @date 2021/12/21 21:56
 * @description
 */
public class UserService {

    public void sayName() {
        System.out.println("my name is eric!!");
    }
}
