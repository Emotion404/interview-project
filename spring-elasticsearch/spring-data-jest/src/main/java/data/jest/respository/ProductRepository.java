package data.jest.respository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import data.jest.dataobject.ESProductDO;

public interface ProductRepository extends ElasticsearchRepository<ESProductDO, Integer> {

}
