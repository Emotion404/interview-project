package cl.demo;

import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;

public class ClassLoaderMain {
    public final static String CLASSLOADER_SERVICE = "/cl-service/";

    public static void main(String[] args) {
        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        try {
            Enumeration<URL> urls = classLoader.getResources(CLASSLOADER_SERVICE);
            while (urls.hasMoreElements()) {
                URL url = urls.nextElement();
                System.out.println(url);
            }
 
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
