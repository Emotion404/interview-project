package jdbc.tokenstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JdbcStoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(JdbcStoreApplication.class, args);
    }
}
