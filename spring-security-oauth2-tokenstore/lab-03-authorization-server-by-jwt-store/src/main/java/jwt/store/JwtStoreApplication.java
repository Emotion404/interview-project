package jwt.store;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JwtStoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(JwtStoreApplication.class, args);
    }
}
