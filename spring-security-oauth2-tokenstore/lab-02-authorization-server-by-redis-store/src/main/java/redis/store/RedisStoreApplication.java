package redis.store;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RedisStoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedisStoreApplication.class, args);
    }
}
