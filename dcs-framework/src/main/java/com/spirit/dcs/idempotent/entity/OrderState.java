package com.spirit.dcs.idempotent.entity;

import lombok.Data;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * Description: 订单状态
 */
@Data
public class OrderState {

    /**
     * id
     */
    private Integer id;

    /**
     * 订单id
     */
    private Long orderId;

    /**
     * token id
     */
    private String tokenId;

    /**
     * 订单状态
     */
    private Integer state;
}
