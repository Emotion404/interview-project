package com.spirit.dcs.idempotent.entity;

import lombok.Data;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * Description: 状态记录表
 */
@Data
public class StateRecord {

    /**
     * id
     */
    private Long id;

    /**
     * 订单id
     */
    private Long orderId;

    /**
     * 状态描述
     */
    private String stateDec;
}
