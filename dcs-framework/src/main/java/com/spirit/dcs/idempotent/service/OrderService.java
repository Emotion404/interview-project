package com.spirit.dcs.idempotent.service;

import com.spirit.dcs.idempotent.entity.OrderState;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * Description:
 */
public interface OrderService {
    /**
     * 查询订单及其状态
     *
     * @param orderState 订单状态
     * @return 订单及其状态
     */
    OrderState queryOrder(OrderState orderState);


    /**
     * 创建订单状态
     *
     * @param orderState 订单状态
     * @return 是否成功
     */
    boolean createOrder(OrderState orderState);

    /**
     * 本地业务
     *
     * @param orderState 订单状态
     * @return 是否成功
     */
    boolean localBiz(OrderState orderState);

    /**
     * 支付业务
     *
     * @param orderState 订单状态
     * @return 是否成功
     */
    boolean paymentBiz(OrderState orderState);

    boolean queryToken(OrderState orderState);
}
