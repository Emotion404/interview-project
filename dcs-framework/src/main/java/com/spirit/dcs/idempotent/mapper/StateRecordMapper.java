package com.spirit.dcs.idempotent.mapper;

import com.spirit.dcs.idempotent.entity.StateRecord;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * Description:
 */
@Mapper
public interface StateRecordMapper {

    void insert(StateRecord stateRecord);
}
