package com.spirit.dcs.idempotent.mapper;

import com.spirit.dcs.idempotent.entity.OrderState;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * Description:
 */
@Mapper
public interface OrderStateMapper {

    /**
     * 产线订单状态
     *
     * @param params 参数
     * @return 订单状态列表
     */
    List<OrderState> selectByMap(Map<String, Object> params);

    /**
     * 更新状态
     *
     * @param orderState 订单状态
     * @return 是否成功
     */
    int updateState(OrderState orderState);

    /**
     * 新增一条状态
     *
     * @param orderState 订单状态
     * @return 是否成功
     */
    int insert(OrderState orderState);

}
