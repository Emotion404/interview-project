package com.spirit.dcs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * Description: 分布式架构相关
 */
@SpringBootApplication
public class DcsApplication {
    public static void main(String[] args) {
        SpringApplication.run(DcsApplication.class, args);
    }
}
