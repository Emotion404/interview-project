package ssph.rw.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import ssph.rw.entity.OrderDO;

@Repository
public interface OrderMapper extends BaseMapper<OrderDO> {

}