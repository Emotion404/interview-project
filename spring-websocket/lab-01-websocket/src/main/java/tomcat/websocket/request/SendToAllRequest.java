package tomcat.websocket.request;

import lombok.Data;
import tomcat.websocket.service.Message;

@Data
public class SendToAllRequest implements Message {

    public static final String TYPE = "SEND_TO_ALL_REQUEST";

    /**
     * 消息编号
     */
    private String             msgId;
    /**
     * 内容
     */
    private String             content;

}