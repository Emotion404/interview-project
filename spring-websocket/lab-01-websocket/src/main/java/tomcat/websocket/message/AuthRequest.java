package tomcat.websocket.message;

import lombok.Getter;
import tomcat.websocket.service.Message;

/**
 * @author zhenglian
 */
@Getter
public class AuthRequest implements Message {

    /**
     * TYPE 静态属性，消息类型为 AUTH_REQUEST 。
     */
    public static final String TYPE = "AUTH_REQUEST";

    /**
     * 认证 Token
     * 在 WebSocket 协议中，我们也需要认证当前连接，用户身份是什么。
     * 一般情况下，我们采用用户调用 HTTP 登录接口，登录成功后返回的访问令牌 accessToken
     */
    private String             accessToken;

    public AuthRequest setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        return this;
    }
}
