package tomcat.websocket.message;

import lombok.Getter;
import tomcat.websocket.service.Message;

/**
 * @author zhenglian
 */
@Getter
public class UserJoinNoticeRequest implements Message {

    public static final String TYPE = "USER_JOIN_NOTICE_REQUEST";

    /**
     * 昵称
     */
    private String             nickname;

    public UserJoinNoticeRequest setNickname(String nickname) {
        this.nickname = nickname;
        return this;
    }
}
