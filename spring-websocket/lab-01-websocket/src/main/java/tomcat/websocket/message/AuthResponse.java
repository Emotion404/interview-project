package tomcat.websocket.message;

import lombok.Getter;
import tomcat.websocket.service.Message;

/**
 * @author zhenglian
 */
@Getter
public class AuthResponse implements Message {

    public static final String TYPE = "AUTH_RESPONSE";

    /**
     * 响应状态码
     */
    private Integer            code;
    /**
     * 响应提示
     */
    private String             message;

    public AuthResponse setCode(Integer code) {
        this.code = code;
        return this;
    }

    public AuthResponse setMessage(String message) {
        this.message = message;
        return this;
    }
}
