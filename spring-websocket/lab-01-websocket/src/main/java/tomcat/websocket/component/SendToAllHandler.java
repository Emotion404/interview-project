package tomcat.websocket.component;// SendToAllRequest.java

import javax.websocket.Session;

import org.springframework.stereotype.Component;

import tomcat.websocket.request.SendToAllRequest;
import tomcat.websocket.request.SendToUserRequest;
import tomcat.websocket.response.SendResponse;
import tomcat.websocket.service.MessagesHandler;
import tomcat.websocket.util.WebSocketUtil;

@Component
public class SendToAllHandler implements MessagesHandler<SendToAllRequest> {

    @Override
    public void execute(Session session, SendToAllRequest message) {
        // 这里，假装直接成功
        SendResponse sendResponse = new SendResponse().setMsgId(message.getMsgId()).setCode(0);
        WebSocketUtil.send(session, SendResponse.TYPE, sendResponse);

        // 创建转发的消息
        SendToUserRequest sendToUserRequest = new SendToUserRequest().setMsgId(message.getMsgId()).setContent(message.getContent());
        // 广播发送
        WebSocketUtil.broadcast(SendToUserRequest.TYPE, sendToUserRequest);
    }

    @Override
    public String getType() {
        return SendToAllRequest.TYPE;
    }

}