package tomcat.websocket.component;// SendToOneRequest.java

import javax.websocket.Session;

import org.springframework.stereotype.Component;

import tomcat.websocket.request.SendToOneRequest;
import tomcat.websocket.request.SendToUserRequest;
import tomcat.websocket.response.SendResponse;
import tomcat.websocket.service.MessagesHandler;
import tomcat.websocket.util.WebSocketUtil;

@Component
public class SendToOneHandler implements MessagesHandler<SendToOneRequest> {

    @Override
    public void execute(Session session, SendToOneRequest message) {
        // 这里，假装直接成功
        SendResponse sendResponse = new SendResponse();
        sendResponse.setMsgId(message.getMsgId());
        sendResponse.setCode(0);
        WebSocketUtil.send(session, SendResponse.TYPE, sendResponse);

        // 创建转发的消息
        SendToUserRequest sendToUserRequest = new SendToUserRequest();
        sendToUserRequest.setMsgId(message.getMsgId());
        sendToUserRequest.setContent(message.getContent());
        // 广播发送
        WebSocketUtil.send(message.getToUser(), SendToUserRequest.TYPE, sendToUserRequest);
    }

    @Override
    public String getType() {
        return SendToOneRequest.TYPE;
    }

}