package websocket.stomp.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

/**
 * 基于stomp 的websocket 本地认证
 */
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketLocalConfig implements WebSocketMessageBrokerConfigurer {

    /**
     * 注册端点
     *
     * @param registry
     */
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/websocket")
                // 跨域
                .setAllowedOrigins("*")
                // websocketjs 方式
                .withSockJS();
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        // 这里使用本地内存方式 注册两个主要是将广播和队列分开
        registry.enableSimpleBroker("/topic", "/queue1");
        // 其他方式（mq）

        // 客户端名称前缀
        registry.setApplicationDestinationPrefixes("/app");
        // 用户名称前缀  服务端通知客户端的前缀，可以不设置，默认为user
        registry.setUserDestinationPrefix("/user");

    }
}
