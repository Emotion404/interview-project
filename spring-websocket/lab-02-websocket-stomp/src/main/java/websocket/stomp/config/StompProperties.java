package websocket.stomp.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@Data
@ConfigurationProperties(prefix = "stomp", ignoreUnknownFields = false)
public class StompProperties {
    private Integer port;
    private String  username;
    private String  password;
    private String  server;
}
