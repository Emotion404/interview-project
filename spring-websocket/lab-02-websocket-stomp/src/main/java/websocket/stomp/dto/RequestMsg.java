package websocket.stomp.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class RequestMsg {


    private String code;
    /**
     * 通知信息体
     */
    private Object body;

}
