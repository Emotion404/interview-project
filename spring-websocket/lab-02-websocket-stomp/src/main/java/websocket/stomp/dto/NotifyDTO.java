package websocket.stomp.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NotifyDTO<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 通知编码<br/>
     * 00-告警<br/>
     * 02-系统公告<br/>
     * 11-请假审批列表刷新<br/>
     * 12-xxx列表状态变更刷新<br/>
     */
    private String            code;
    /**
     * 通知信息体
     */
    private T                 data;

    /**
     * 推送哪些人
     */
    private List<Integer>     toUserIds;
}
