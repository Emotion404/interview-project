package spirit.io.bio;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.UUID;

/**
 * 同步阻塞io模型
 */
public class BioClient {

    public static void main(String[] args) {
        int count = 100;

        try {
            Socket client = new Socket("localhost", 8080);
            // 不管是客户端和服务端， 都有可能read and write
            OutputStream os = client.getOutputStream();

            String id = UUID.randomUUID().toString();
            System.out.println("客户端发送数据：" + id);

            // 转成二进制流
            os.write(id.getBytes());

            os.close();
            client.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
