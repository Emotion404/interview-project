package demo.bio.server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * @description bio demo
 */
public class BioServer extends Thread {

    private ServerSocket serverSocket;

    public static void main(String[] args) {
        new BioServer().start();
    }

    @Override
    public void run() {
        try {
            serverSocket = new ServerSocket();
            serverSocket.bind(new InetSocketAddress(7397));
            System.out.println("BIO SERVER DEMO FINISH INIT...");
            
            while (true) {
                Socket socket = serverSocket.accept();
                BioServerHandler bioServerHandler = new BioServerHandler(socket, Charset.forName("utf-8"));
                bioServerHandler.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
