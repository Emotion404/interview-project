package zheng.interview;

/**
 * @author eric
 * @date 2021/12/25 21:22
 * @description
 */
public class Main {

    public static void main(String[] args) {
        System.out.println("hello service");
        IHelloService helloService = new RpcClientProxy()
                .clientProxy(IHelloService.class, "localhost", 8080);

        String result = helloService.sayHello("ERIC");
        System.out.println(result);
    }
}
