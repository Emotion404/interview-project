package zheng.interview;

import java.lang.reflect.Proxy;

/**
 * @author eric
 * @date 2021/12/25 21:23
 * @description
 */
public class RpcClientProxy {

    /**
     * 创建客户端代理
     * 此代理作用是 在调用远程api的时候 创建socket通信，获取远程服务端的运行的结果
     *
     * @param interfaceCls 被代理的对象
     * @param host         远程服务的地址
     * @param port         远程服务的端口
     * @param <T>          返回的类型
     * @return 返回实例对象
     */
    public <T> T clientProxy(final Class<T> interfaceCls, final String host, final int port) {
        return (T) Proxy.newProxyInstance(interfaceCls.getClassLoader(), new Class<?>[]{interfaceCls},
                new RemoteInvocationHandler(host, port));
    }

}
