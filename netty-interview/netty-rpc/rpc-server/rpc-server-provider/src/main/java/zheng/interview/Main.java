package zheng.interview;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import zheng.interview.config.SpringConfig;

/**
 * @author eric
 * @date 2021/12/25 21:17
 * @description
 */
public class Main {

    // 发布服务
    public static void main(String[] args) {
//        HelloServiceImpl helloService = new HelloServiceImpl();
//        RpcProxyServer rpcProxyServer = new RpcProxyServer();
//        rpcProxyServer.publisher(helloService, 8080);
        ApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        ((AnnotationConfigApplicationContext) context).start();
    }
}
