package zheng.interview.annotation;

import org.springframework.stereotype.Component;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author eric
 * @date 2021/12/25 22:12
 * @description
 */
// 类或者接口
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
// 可以被spring 进行解析
@Component
public @interface RpcService {
    /**
     * 拿到服务的接口
     */
    Class<?> value();

    /**
     * 版本号
     *
     * @return 版本号
     */
    String version() default "";
}
