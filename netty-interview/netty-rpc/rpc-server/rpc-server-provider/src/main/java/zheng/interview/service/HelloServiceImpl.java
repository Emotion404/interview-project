package zheng.interview.service;

import zheng.interview.IHelloService;
import zheng.interview.User;
import zheng.interview.annotation.RpcService;

/**
 * @author eric
 * @date 2021/12/25 20:50
 * @description
 */
@RpcService(value = IHelloService.class, version = "v1.0")
public class HelloServiceImpl implements IHelloService {
    @Override
    public String sayHello(String content) {
        System.out.println("[v1.0] request in sayHello" + content);
        return "[v2.0] say hello " + content;
    }

    @Override
    public String saveUser(User user) {
        System.out.println("[v1.0]request in saveUser" + user);
        return "[v1.0]SUCCESS";
    }
}
