package zheng.interview.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import zheng.interview.rpc.RpcServer;

/**
 * @author eric
 * @date 2021/12/25 22:20
 * @description
 */
@Configuration
@ComponentScan(basePackages = "zheng.interview")
public class SpringConfig {

    @Bean(name = "rpcServer")
    public RpcServer rpcServer() {
        return new RpcServer(8080);
    }
}
