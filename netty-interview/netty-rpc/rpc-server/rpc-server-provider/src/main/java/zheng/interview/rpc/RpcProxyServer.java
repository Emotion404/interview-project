package zheng.interview.rpc;


import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author eric
 * @date 2021/12/25 20:54
 * @description 暴露服务的代理
 */
public class RpcProxyServer {

    ExecutorService executorService = Executors.newCachedThreadPool();

    public void publisher(Object service, int port) {
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(port);
            // 不断接受请求
//            while (true) {
//                Socket socket = serverSocket.accept();
            // 每一个socket 交给一个 processorHandler 来处理
//                executorService.execute(new ProcessorHandler(service, socket));
//            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
