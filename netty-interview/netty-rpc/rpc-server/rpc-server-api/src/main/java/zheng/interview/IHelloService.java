package zheng.interview;

/**
 * @author eric
 * @date 2021/12/25 20:47
 * @description
 */
public interface IHelloService {
    String sayHello(String content);

    String saveUser(User user);
}
