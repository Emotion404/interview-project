package zheng.interview;

import lombok.Data;

import java.io.Serializable;

/**
 * @author eric
 * @date 2021/12/25 21:02
 * @description
 */
@Data
public class RpcRequest implements Serializable {

    private String className;

    private String methodName;

    private Object[] parameters;

    private String version;
}
