package zheng.interview;

import lombok.Data;
import lombok.ToString;

/**
 * @author eric
 * @date 2021/12/25 20:48
 * @description
 */
@Data
@ToString
public class User {

    private String name;

    private int age;


}
