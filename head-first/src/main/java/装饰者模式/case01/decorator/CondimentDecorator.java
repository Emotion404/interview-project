package 装饰者模式.case01.decorator;

import 装饰者模式.case01.Beverage;

/**
 * @author zhenglian
 * 调料装饰者
 * 核心部分：需要继承Beverage抽象类， 其目的并不是扩展父类，而是为了类型匹配
 */
public abstract class CondimentDecorator extends Beverage {

    public abstract String getDescription();
}
