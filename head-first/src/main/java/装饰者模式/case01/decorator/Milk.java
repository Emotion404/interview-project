package 装饰者模式.case01.decorator;

import 装饰者模式.case01.Beverage;

/**
 * @author zhenglian
 * mocha 装饰者
 */
public class Milk extends CondimentDecorator {

    // 传入一个被包装的对象
    private Beverage beverage;

    public Milk(Beverage beverage) {
        this.beverage = beverage;
    }

    @Override
    public double cost() {
        return 5.00 + beverage.cost();
    }

    @Override
    public String getDescription() {
        String description = beverage.getDescription() + " 加入了： 牛奶调味料 \n ";
        return description;
    }
}
