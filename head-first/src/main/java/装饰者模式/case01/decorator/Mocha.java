package 装饰者模式.case01.decorator;

import 装饰者模式.case01.Beverage;

/**
 * @author zhenglian
 * mocha 装饰者
 */
public class Mocha extends CondimentDecorator {

    private Beverage beverage;

    public Mocha(Beverage beverage) {
        this.beverage = beverage;
    }

    @Override
    public double cost() {
        return 3.00 + beverage.cost();
    }

    @Override
    public String getDescription() {
        String description = beverage.getDescription() + " 加入了： 摩卡调味料 \n";
        return description;
    }
}
