package 装饰者模式.case01;

/**
 * @author zhenglian
 * <p>
 * 这是一个顶级的抽象类，每个类都要实现cost
 * 还有其他自身的方法
 */
public abstract class Beverage {

    public String description;

    public abstract double cost();

    public String getDescription() {
        return this.description;
    }

    // ..... other methods.
}
