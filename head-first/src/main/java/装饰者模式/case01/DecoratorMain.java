package 装饰者模式.case01;

import 装饰者模式.case01.coffie.Decaf;
import 装饰者模式.case01.decorator.CondimentDecorator;
import 装饰者模式.case01.decorator.Milk;
import 装饰者模式.case01.decorator.Mocha;

/**
 * 装饰者该做的事就是增加行为到被包装的对象上
 */
public class DecoratorMain {

    public static void main(String[] args) {
        Beverage decaf = new Decaf();
        CondimentDecorator milk = new Milk(decaf);
        CondimentDecorator mocha = new Mocha(milk);
        System.out.println(mocha.getDescription() + "价钱为： " + mocha.cost());
    }
}
