package 装饰者模式.case01.coffie;

import 装饰者模式.case01.Beverage;

/**
 * @author zhenglian
 */
public class DarkRoast extends Beverage {

    public DarkRoast() {
        super.description = "Dark Roast";
    }

    @Override
    public double cost() {
        return 8.00;
    }
}
