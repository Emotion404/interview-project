package 装饰者模式.case01.coffie;

import 装饰者模式.case01.Beverage;

public class Decaf extends Beverage {

    public Decaf() {
        super.description = "Decaf";
    }

    @Override
    public double cost() {
        return 13.00;
    }
}
