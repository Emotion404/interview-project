package 装饰者模式.case01.coffie;

import 装饰者模式.case01.Beverage;

public class HouseBlend extends Beverage {

    public HouseBlend() {
        super.description = "House Blend";
    }

    @Override
    public double cost() {
        return 15.00;
    }
}
