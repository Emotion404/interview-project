package 装饰者模式.case01.coffie;

import 装饰者模式.case01.Beverage;

public class Espresso extends Beverage {

    public Espresso() {
        super.description = "Espresso";
    }

    @Override
    public double cost() {
        return 18.00;
    }
}
