package 装饰者模式.case02.jdkcase;

import java.io.*;

public class IoMain {

    public static void main(String[] args) {
        try {
            BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(new File("D:\\decorator.txt")));
            int ioSize = bufferedInputStream.read();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
