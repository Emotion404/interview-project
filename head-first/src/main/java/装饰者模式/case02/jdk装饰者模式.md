## jdk中使用到的装饰者模式
- InputStream  
比如说BufferedInputStream就是一个装饰流，可以用它装饰FileInputStream，  
所以我们最常用的应该是这样的形式：new BufferedInputStream(new FileInputStream(new File("")));  
和我们上面讲的类似，装饰流也是增加一些行为到被装饰的对象上，  
比如BufferedInputStream通过缓冲数组来提高性能，提供一个读取整行的readLine方法来进行扩展。  
下面的图能更加深入了解IO流中的装饰者：  
![InputStream]("https://images2015.cnblogs.com/blog/963707/201703/963707-20170317105258370-1945391682.png")

- ...