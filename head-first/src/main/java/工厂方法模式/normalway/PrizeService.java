package 工厂方法模式.normalway;

import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import 工厂方法模式.common.AwardReq;
import 工厂方法模式.common.AwardRes;
import 工厂方法模式.common.card.IQiYiCardService;
import 工厂方法模式.common.coupon.CouponResult;
import 工厂方法模式.common.coupon.CouponService;
import 工厂方法模式.common.goods.DeliverReq;
import 工厂方法模式.common.goods.GoodsService;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * Description:
 */
public class PrizeService {
    private Logger logger = LoggerFactory.getLogger(PrizeService.class);

    public AwardRes awardToUser(AwardReq req) {
        String reqJson = JSON.toJSONString(req);
        AwardRes awardRes = null;
        try {
            logger.info("奖品发放开始{}。req:{}", req.getUid(), reqJson);
            // 按照不同类型⽅法商品[1优惠券、2实物商品、3第三⽅兑换卡(爱奇艺)]
            if (req.getAwardType() == 1) {
                CouponService couponService = new CouponService();
                CouponResult couponResult = couponService.sendCoupon(req.getUid(), req.getAwardNumber(),
                        req.getBizId());
                if ("0000".equals(couponResult.getCode())) {
                    awardRes = new AwardRes("0000", "发放成功");
                } else {
                    awardRes = new AwardRes("0001", couponResult.getInfo());
                }
            } else if (req.getAwardType() == 2) {
                GoodsService goodsService = new GoodsService();
                DeliverReq deliverReq = new DeliverReq();
                deliverReq.setUserName(queryUserName(req.getUid()));

                deliverReq.setUserPhone(queryUserPhoneNumber(req.getUid()));

                deliverReq.setSku(req.getAwardNumber());
                deliverReq.setOrderId(req.getBizId());

                deliverReq.setConsigneeUserName(req.getExtMap().get("consigneeUserName"));

                deliverReq.setConsigneeUserPhone(req.getExtMap().get("consigneeUserPhone"));

                deliverReq.setConsigneeUserAddress(req.getExtMap().get("consigneeUserAddress"));
                Boolean isSuccess = goodsService.deliverGoods(deliverReq);
                if (isSuccess) {
                    awardRes = new AwardRes("0000", "发放成功");
                } else {
                    awardRes = new AwardRes("0001", "发放失败");
                }
            } else if (req.getAwardType() == 3) {
                String bindMobileNumber = queryUserPhoneNumber(req.getUid());
                IQiYiCardService iQiYiCardService = new IQiYiCardService();
                iQiYiCardService.grantToken(bindMobileNumber, req.getAwardNumber());
                awardRes = new AwardRes("0000", "发放成功");
            }
            logger.info("奖品发放完成{}。", req.getUid());
        } catch (Exception e) {
            logger.error("奖品发放失败{}。req:{}", req.getUid(), reqJson, e);
            awardRes = new AwardRes("0001", e.getMessage());
        }
        return awardRes;
    }

    private String queryUserName(String uId) {
        return "花花";
    }

    private String queryUserPhoneNumber(String uId) {
        return "15200101232";
    }
}
