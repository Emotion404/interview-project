package 工厂方法模式.common;

import lombok.Data;

import java.util.Map;

/**
 * @author <a href="eric_zheng@lanzuo.com.cn">eric</a>
 * @version 1.0.0
 * Description:
 */
@Data
public class AwardReq {

    /**
     * 用户唯一id
     */
    private String uid;

    /**
     * 奖品类型(可以用枚举定义)；1优惠券、2实物商品、3第三方兑换卡(爱奇艺)
     */
    private Integer awardType;

    /**
     * 奖品编号；sku、couponNumber、cardId
     */
    private String awardNumber;

    /**
     * 业务id 防重复
     */
    private String bizId;

    /**
     * 拓展信息
     */
    private Map<String, String> extMap;
}
