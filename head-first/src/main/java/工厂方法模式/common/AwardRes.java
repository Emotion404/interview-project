package 工厂方法模式.common;

import lombok.Data;

/**
 * @author eric
 */
@Data
public class AwardRes {
    // 编码
    private String code;
    // 描述
    private String info;

    public AwardRes(String code, String info) {
        this.code = code;
        this.info = info;
    }

}